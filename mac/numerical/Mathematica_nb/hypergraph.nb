(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     27672,        715]
NotebookOptionsPosition[     26477,        674]
NotebookOutlinePosition[     26831,        690]
CellTagsIndexPosition[     26788,        687]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"{", 
    RowBox[{"k", ",", "n"}], "}"}], "=", 
   RowBox[{"{", 
    RowBox[{"3", ",", "5"}], "}"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.5718054052405376`*^9, 3.57180547048064*^9}, {
   3.571805553028508*^9, 3.571805554394586*^9}, 3.571806160659309*^9, 
   3.57180913793227*^9, 3.5718094928918266`*^9, 3.5718611148899746`*^9}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"f", "[", 
   RowBox[{"A_", ",", "x_"}], "]"}], ":=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"Sum", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"A", "[", 
         RowBox[{"[", 
          RowBox[{"i", ",", "i2", ",", "i3"}], "]"}], "]"}], 
        RowBox[{"x", "[", 
         RowBox[{"[", "i2", "]"}], "]"}], 
        RowBox[{"x", "[", 
         RowBox[{"[", "i3", "]"}], "]"}]}], ",", 
       RowBox[{"{", 
        RowBox[{"i2", ",", "1", ",", "n"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"i3", ",", "1", ",", "n"}], "}"}]}], "]"}], "+", 
     RowBox[{
      RowBox[{"x", "[", 
       RowBox[{"[", "i", "]"}], "]"}], "^", 
      RowBox[{"(", 
       RowBox[{"k", "-", "1"}], ")"}]}]}], ",", 
    RowBox[{"{", 
     RowBox[{"i", ",", "1", ",", "n"}], "}"}]}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"g1", "[", "x_", "]"}], ":=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"x", "[", 
      RowBox[{"[", "i", "]"}], "]"}], "^", 
     RowBox[{"(", 
      RowBox[{"1", "/", 
       RowBox[{"(", 
        RowBox[{"k", "-", "1"}], ")"}]}], ")"}]}], ",", 
    RowBox[{"{", 
     RowBox[{"i", ",", "1", ",", "n"}], "}"}]}], "]"}]}], "\n", 
 RowBox[{
  RowBox[{"g2", "[", "x_", "]"}], ":=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"x", "[", 
      RowBox[{"[", "i", "]"}], "]"}], "^", 
     RowBox[{"(", 
      RowBox[{"k", "-", "1"}], ")"}]}], ",", 
    RowBox[{"{", 
     RowBox[{"i", ",", "1", ",", "n"}], "}"}]}], "]"}]}]}], "Input",
 CellChangeTimes->{{3.571806374031538*^9, 3.571806400484646*^9}, {
  3.5718064878215766`*^9, 3.5718064929522667`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"edgeset", "=", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"1", ",", "2", ",", "3"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"1", ",", "4", ",", "5"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"2", ",", "4", ",", "5"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"3", ",", "4", ",", "5"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"4", ",", "5", ",", "6"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"4", ",", "5", ",", "7"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"4", ",", "5", ",", "8"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"6", ",", "7", ",", "8"}], "}"}]}], "}"}]}], ";"}], "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"edgeset", "=", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"1", ",", "2", ",", "3"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"3", ",", "4", ",", "5"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"1", ",", "2", ",", "5"}], "}"}]}], "}"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"degree", "=", 
     RowBox[{"Table", "[", 
      RowBox[{
       RowBox[{"Count", "[", 
        RowBox[{
         RowBox[{"Flatten", "[", "edgeset", "]"}], ",", "i"}], "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"i", ",", "n"}], "}"}]}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"{", 
      RowBox[{"dmax", ",", "dmin", ",", "daverage"}], "}"}], "=", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"Max", "[", "degree", "]"}], ",", 
       RowBox[{"Min", "[", "degree", "]"}], ",", 
       RowBox[{"Mean", "[", "degree", "]"}]}], "}"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"indiceset", "=", 
     RowBox[{"Flatten", "[", 
      RowBox[{
       RowBox[{"Map", "[", 
        RowBox[{"Permutations", ",", "edgeset"}], "]"}], ",", "1"}], "]"}]}], 
    ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"indiceA", "=", 
     RowBox[{"Array", "[", 
      RowBox[{
       RowBox[{"n", "&"}], ",", 
       RowBox[{"{", "k", "}"}]}], "]"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"tensor", "=", 
     RowBox[{"Array", "[", 
      RowBox[{
       RowBox[{"0", "&"}], ",", "indiceA"}], "]"}]}], ";"}]}]}]], "Input",
 CellChangeTimes->{{3.5718055737684917`*^9, 3.5718057511613445`*^9}, {
   3.5718058026212487`*^9, 3.571805855611452*^9}, {3.5718058890231433`*^9, 
   3.5718059952187557`*^9}, 3.571806027585995*^9, {3.5718061442733803`*^9, 
   3.5718061698666286`*^9}, {3.5718062223603764`*^9, 
   3.5718062320631294`*^9}, {3.571806664601183*^9, 3.57180687279416*^9}, 
   3.5718069338534193`*^9, {3.5718071588741684`*^9, 3.571807161711727*^9}, 
   3.571807213364233*^9, {3.5718078417865763`*^9, 3.5718078442207155`*^9}, {
   3.571807946666532*^9, 3.571807955394225*^9}, {3.571807994823662*^9, 
   3.571808066949938*^9}, {3.5718093374356375`*^9, 3.57180936603026*^9}, {
   3.571809435472991*^9, 3.571809489340826*^9}, {3.5718611195374384`*^9, 
   3.571861128221328*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"diagonalindice", "=", 
   RowBox[{"Array", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"{", 
       RowBox[{"#", ",", "#", ",", "#"}], "}"}], "&"}], ",", "n"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"ruleA", "=", 
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"indiceset", "[", 
       RowBox[{"[", "i", "]"}], "]"}], "\[Rule]", 
      RowBox[{"1", "/", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"k", "-", "1"}], ")"}], "!"}]}]}], ",", 
     RowBox[{"{", 
      RowBox[{"i", ",", 
       RowBox[{"Length", "[", "indiceset", "]"}]}], "}"}]}], "]"}]}], 
  ";"}], "\n", 
 RowBox[{
  RowBox[{"tensorA", "=", 
   RowBox[{"ReplacePart", "[", 
    RowBox[{"tensor", ",", "ruleA"}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"ruleD", "=", 
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"diagonalindice", "[", 
       RowBox[{"[", "i", "]"}], "]"}], "\[Rule]", 
      RowBox[{"degree", "[", 
       RowBox[{"[", "i", "]"}], "]"}]}], ",", 
     RowBox[{"{", 
      RowBox[{"i", ",", "n"}], "}"}]}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"tensorD", "=", 
   RowBox[{"ReplacePart", "[", 
    RowBox[{"tensor", ",", "ruleD"}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"ruleI", "=", 
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"diagonalindice", "[", 
       RowBox[{"[", "i", "]"}], "]"}], "\[Rule]", "1"}], ",", 
     RowBox[{"{", 
      RowBox[{"i", ",", "n"}], "}"}]}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"tensorI", "=", 
   RowBox[{"ReplacePart", "[", 
    RowBox[{"tensor", ",", "ruleI"}], "]"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.5718055737684917`*^9, 3.5718057511613445`*^9}, {
   3.5718058026212487`*^9, 3.571805855611452*^9}, {3.5718058890231433`*^9, 
   3.5718059952187557`*^9}, 3.571806027585995*^9, {3.5718061442733803`*^9, 
   3.5718061698666286`*^9}, {3.5718062223603764`*^9, 
   3.5718062320631294`*^9}, {3.571806664601183*^9, 3.5718069208740864`*^9}, {
   3.5718069648845778`*^9, 3.5718072211954746`*^9}, {3.571807773589525*^9, 
   3.571807774442574*^9}, {3.5718078494056115`*^9, 3.571807880808597*^9}, {
   3.571807911610549*^9, 3.571807976898839*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"tensorQ", "=", 
   RowBox[{"tensorD", "+", "tensorA"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"tensorL", "=", 
   RowBox[{"tensorD", "-", "tensorA"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"tensorLp", "=", 
   RowBox[{"tensorA", "-", "tensorD", "+", 
    RowBox[{"dmax", "*", "tensorI"}]}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.571806811082678*^9, 3.5718068226093283`*^9}, {
  3.571807225105896*^9, 3.571807246092868*^9}, {3.5718073526854906`*^9, 
  3.571807358939844*^9}, {3.5718077284301753`*^9, 3.5718077545816493`*^9}, {
  3.571807984311263*^9, 3.571807991030444*^9}, {3.5718080717298098`*^9, 
  3.571808153460045*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"eigenpair", "[", "A_", "]"}], ":=", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"x", " ", "=", " ", 
     RowBox[{"Array", "[", 
      RowBox[{
       RowBox[{"#", "&"}], ",", "n"}], "]"}]}], ";", "\[IndentingNewLine]", 
    RowBox[{"tList", " ", "=", " ", 
     RowBox[{"tlist", " ", "=", " ", 
      RowBox[{"Array", "[", 
       RowBox[{
        RowBox[{"0", " ", "&"}], ",", "3000"}], "]"}]}]}], ";", 
    "\[IndentingNewLine]", 
    RowBox[{"t", "=", 
     RowBox[{"tla", "=", 
      RowBox[{"{", 
       RowBox[{"2", ",", "1"}], "}"}]}]}], ";", "\[IndentingNewLine]", 
    RowBox[{"xa", "=", 
     RowBox[{"Array", "[", 
      RowBox[{
       RowBox[{"1", "&"}], ",", "n"}], "]"}]}], ";", "\[IndentingNewLine]", 
    RowBox[{"xb", "=", 
     RowBox[{"Array", "[", 
      RowBox[{
       RowBox[{"2", "&"}], ",", "n"}], "]"}]}], ";", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"Timing", "[", 
      RowBox[{"For", "[", 
       RowBox[{
        RowBox[{"j", "=", "0"}], ",", 
        RowBox[{
         RowBox[{
          RowBox[{"Max", "[", 
           RowBox[{
            RowBox[{"Abs", "[", 
             RowBox[{
              RowBox[{"tla", "[", 
               RowBox[{"[", "2", "]"}], "]"}], "-", 
              RowBox[{"tla", "[", 
               RowBox[{"[", "1", "]"}], "]"}]}], "]"}], ",", 
            RowBox[{"Norm", "[", 
             RowBox[{"xa", "-", "xb"}], "]"}]}], "]"}], "\[GreaterEqual]", 
          RowBox[{"10", "^", 
           RowBox[{"-", "6"}]}]}], "&&", 
         RowBox[{"j", "\[LessEqual]", "5000"}]}], ",", 
        RowBox[{"j", "++"}], ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{
           RowBox[{"tla", "[", 
            RowBox[{"[", "1", "]"}], "]"}], "=", 
           RowBox[{"t", "[", 
            RowBox[{"[", "2", "]"}], "]"}]}], ",", 
          RowBox[{"xa", "=", "x"}], ",", "\[IndentingNewLine]", 
          RowBox[{"y", "=", 
           RowBox[{"f", "[", 
            RowBox[{"A", ",", "x"}], "]"}]}], ",", "\n", 
          RowBox[{"x", "=", 
           RowBox[{
            RowBox[{
             RowBox[{"g1", "[", "y", "]"}], "/", 
             RowBox[{"Norm", "[", 
              RowBox[{"g1", "[", "y", "]"}], "]"}]}], "//", "N"}]}], ",", 
          RowBox[{"xb", "=", "x"}], ",", "\n", 
          RowBox[{"y", "=", 
           RowBox[{
            RowBox[{"f", "[", 
             RowBox[{"A", ",", "x"}], "]"}], "//", "N"}]}], ",", "\n", 
          RowBox[{"t", "=", 
           RowBox[{
            RowBox[{"{", 
             RowBox[{
              RowBox[{"Min", "[", 
               RowBox[{"Table", "[", 
                RowBox[{
                 RowBox[{"If", "[", 
                  RowBox[{
                   RowBox[{
                    RowBox[{
                    RowBox[{"g2", "[", "x", "]"}], "[", 
                    RowBox[{"[", "i", "]"}], "]"}], ">", "0"}], ",", 
                   RowBox[{
                    RowBox[{"y", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], "/", 
                    RowBox[{
                    RowBox[{"g2", "[", "x", "]"}], "[", 
                    RowBox[{"[", "i", "]"}], "]"}]}], ",", "Infinity"}], 
                  "]"}], ",", 
                 RowBox[{"{", 
                  RowBox[{"i", ",", "1", ",", "n"}], "}"}]}], "]"}], "]"}], 
              ",", 
              RowBox[{"Max", "[", 
               RowBox[{"Table", "[", 
                RowBox[{
                 RowBox[{"If", "[", 
                  RowBox[{
                   RowBox[{
                    RowBox[{
                    RowBox[{"g2", "[", "x", "]"}], "[", 
                    RowBox[{"[", "i", "]"}], "]"}], ">", "0"}], ",", 
                   RowBox[{
                    RowBox[{"y", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], "/", 
                    RowBox[{
                    RowBox[{"g2", "[", "x", "]"}], "[", 
                    RowBox[{"[", "i", "]"}], "]"}]}], ",", 
                   RowBox[{"-", "Infinity"}]}], "]"}], ",", 
                 RowBox[{"{", 
                  RowBox[{"i", ",", "1", ",", "n"}], "}"}]}], "]"}], "]"}]}], 
             "}"}], "//", "N"}]}], ",", 
          RowBox[{
           RowBox[{"tlist", "[", 
            RowBox[{"[", 
             RowBox[{"j", "+", "1"}], "]"}], "]"}], "=", 
           RowBox[{"t", "[", 
            RowBox[{"[", "1", "]"}], "]"}]}], ",", 
          RowBox[{
           RowBox[{"tList", "[", 
            RowBox[{"[", 
             RowBox[{"j", "+", "1"}], "]"}], "]"}], "=", 
           RowBox[{"t", "[", 
            RowBox[{"[", "2", "]"}], "]"}]}], ",", 
          RowBox[{
           RowBox[{"tla", "[", 
            RowBox[{"[", "2", "]"}], "]"}], "=", 
           RowBox[{"t", "[", 
            RowBox[{"[", "2", "]"}], "]"}]}]}], 
         RowBox[{"(*", 
          RowBox[{",", 
           RowBox[{"Print", "[", 
            RowBox[{"{", 
             RowBox[{"x", ",", "y", ",", "t"}], "}"}], "]"}]}], "*)"}], 
         "}"}]}], "]"}], "]"}], "\[IndentingNewLine]", 
     RowBox[{"Print", "[", 
      RowBox[{"\"\<iterative=\>\"", ",", 
       RowBox[{"j", "-", "1"}], ",", "\"\<      convergence value=\>\"", ",", 
       RowBox[{
        RowBox[{"tla", "[", 
         RowBox[{"[", "2", "]"}], "]"}], "-", "1"}]}], "]"}], 
     RowBox[{
     "(*", "\:8fed\:4ee3\:6b21\:6570\:548c\:6536\:655b\:5230\:7684\:7279\:5f81\
\:503c", "*)"}], "\[IndentingNewLine]", 
     RowBox[{"Print", "[", 
      RowBox[{"Chop", "[", "x", "]"}], "]"}], 
     RowBox[{"(*", "\:6536\:655b\:5230\:7684\:7279\:5f81\:5411\:91cf", "*)"}],
      "\[IndentingNewLine]", 
     RowBox[{"Print", "[", 
      RowBox[{"ListPlot", "[", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"tlist", "[", 
          RowBox[{"[", 
           RowBox[{"1", ";;", 
            RowBox[{"j", "+", "1"}]}], "]"}], "]"}], ",", 
         RowBox[{"tList", "[", 
          RowBox[{"[", 
           RowBox[{"1", ";;", 
            RowBox[{"j", "+", "1"}]}], "]"}], "]"}]}], "}"}], "]"}], 
      RowBox[{"(*", "\:4e0a\:4e0b\:754c\:7684\:56fe\:793a", "*)"}], "]"}]}]}],
    ")"}]}]], "Input",
 CellChangeTimes->{{3.571806404959096*^9, 3.571806430032323*^9}, {
   3.5718064657399387`*^9, 3.571806469569955*^9}, {3.571806567489293*^9, 
   3.571806618497158*^9}, {3.5718072637314672`*^9, 3.5718073259031744`*^9}, 
   3.5718073760990148`*^9, {3.5718074077994056`*^9, 3.5718074148350067`*^9}, {
   3.571807519645531*^9, 3.571807597435726*^9}, {3.5718083415205426`*^9, 
   3.5718083417915583`*^9}, {3.571809550214674*^9, 3.571809578898698*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Map", "[", 
  RowBox[{"eigenpair", ",", 
   RowBox[{"{", 
    RowBox[{"tensorA", ",", "tensorQ", ",", "tensorLp"}], "}"}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.5718073096782722`*^9, 3.571807316178838*^9}, {
  3.5718074413607044`*^9, 3.571807500069023*^9}, {3.5718081431258593`*^9, 
  3.571808149213005*^9}, {3.5718082616142006`*^9, 3.5718082648585844`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"iterative=\"\>", "\[InvisibleSpace]", "19", 
   "\[InvisibleSpace]", "\<\"      convergence value=\"\>", 
   "\[InvisibleSpace]", "1.872128645746563`"}],
  SequenceForm[
  "iterative=", 19, "      convergence value=", 1.872128645746563],
  Editable->False]], "Print",
 CellChangeTimes->{{3.5718073878676753`*^9, 3.571807417617963*^9}, 
   3.5718075008322654`*^9, 3.571807533602523*^9, {3.571807586808528*^9, 
   3.5718076011293297`*^9}, 3.571808160452837*^9, {3.571808345998195*^9, 
   3.571808371661249*^9}, 3.571809381760548*^9, 3.571809519134513*^9, {
   3.5718095538122764`*^9, 3.571809582174282*^9}, 3.571861148643073*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "0.4867655290051176`", ",", "0.4867655290051176`", ",", 
   "0.4556438453062219`", ",", "0.33301052838287365`", ",", 
   "0.45564384530623814`"}], "}"}]], "Print",
 CellChangeTimes->{{3.5718073878676753`*^9, 3.571807417617963*^9}, 
   3.5718075008322654`*^9, 3.571807533602523*^9, {3.571807586808528*^9, 
   3.5718076011293297`*^9}, 3.571808160452837*^9, {3.571808345998195*^9, 
   3.571808371661249*^9}, 3.571809381760548*^9, 3.571809519134513*^9, {
   3.5718095538122764`*^9, 3.571809582174282*^9}, 3.5718611486460733`*^9}],

Cell[BoxData[
 GraphicsBox[{{}, {
    {RGBColor[0.24720000000000017`, 0.24, 0.6], PointBox[CompressedData["
1:eJxTTMoPSmViYGAQBWIQDQEf7D1LmZdI/2RwgAo4/M/uUNz/lxHK53C4sfCK
U80CZihfwMErfPYZKz5WKF/EYcHP1ZEeP2F8CYepP9euDapig/JlHB4p1+1m
3gPjKzh83XWcx+MOjK/kkLTn7fY7r2F8FYe0tSdEgz/D+GoOezYxNjz6BuNr
ODT2V7RO+QHjazkUbrjDmvwTxtdx+CVea30Uztdz2CGo8u4znG/gsOX8J0Wu
XzC+oYNCj+4GETjfyCHP9s4WSTjf2OHEgTniMnC+icMxkR+WsnC+KSzcGAAu
9FRW
      "]]}, 
    {RGBColor[0.6, 0.24, 0.4428931686004542], PointBox[CompressedData["
1:eJxTTMoPSmViYGAQBWIQDQEf7DPuLDilZyPiABVwEIpl3ifaxA3lczgY/Ns+
m20VB5Qv4PAlxU500252KF/E4d1kK8MjITC+hMNr9XPMv1RgfBmHtvC1gUV8
ML6Cg9g/kwNHmWF8JYflBbtWbPnHBuWrOOy5+LzI5w+Mr+ZwPrJ4hvpvGF/D
QTthasqcXzC+lkP87wCLKDhfx0FJTeywFZyv5zBx5/Rz2nC+gYOFZssmFTjf
0EHFxy5QEc43cpjg+HirPJxv7FCwNPiTHJxv4sAnHj0VwTeFhRsDAPNxTLk=

      "]]}}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  Method->{},
  PlotRange->{{0, 21.}, {0, 2.9775130450585543`}},
  PlotRangeClipping->True,
  PlotRangePadding->{{0.42, 0.42}, {0.05955026090117109, 
   0.05955026090117109}}]], "Print",
 CellChangeTimes->{{3.5718073878676753`*^9, 3.571807417617963*^9}, 
   3.5718075008322654`*^9, 3.571807533602523*^9, {3.571807586808528*^9, 
   3.5718076011293297`*^9}, 3.571808160452837*^9, {3.571808345998195*^9, 
   3.571808371661249*^9}, 3.571809381760548*^9, 3.571809519134513*^9, {
   3.5718095538122764`*^9, 3.571809582174282*^9}, 3.5718611490660973`*^9}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"iterative=\"\>", "\[InvisibleSpace]", "29", 
   "\[InvisibleSpace]", "\<\"      convergence value=\"\>", 
   "\[InvisibleSpace]", "3.8130660710116713`"}],
  SequenceForm[
  "iterative=", 29, "      convergence value=", 3.8130660710116713`],
  Editable->False]], "Print",
 CellChangeTimes->{{3.5718073878676753`*^9, 3.571807417617963*^9}, 
   3.5718075008322654`*^9, 3.571807533602523*^9, {3.571807586808528*^9, 
   3.5718076011293297`*^9}, 3.571808160452837*^9, {3.571808345998195*^9, 
   3.571808371661249*^9}, 3.571809381760548*^9, 3.571809519134513*^9, {
   3.5718095538122764`*^9, 3.571809582174282*^9}, 3.5718611491052976`*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "0.5040651457439117`", ",", "0.504065145743958`", ",", 
   "0.4569517011538197`", ",", "0.2724462024808387`", ",", 
   "0.4569517121739359`"}], "}"}]], "Print",
 CellChangeTimes->{{3.5718073878676753`*^9, 3.571807417617963*^9}, 
   3.5718075008322654`*^9, 3.571807533602523*^9, {3.571807586808528*^9, 
   3.5718076011293297`*^9}, 3.571808160452837*^9, {3.571808345998195*^9, 
   3.571808371661249*^9}, 3.571809381760548*^9, 3.571809519134513*^9, {
   3.5718095538122764`*^9, 3.571809582174282*^9}, 3.571861149120898*^9}],

Cell[BoxData[
 GraphicsBox[{{}, {
    {RGBColor[0.24720000000000017`, 0.24, 0.6], PointBox[CompressedData["
1:eJxTTMoPSmViYGCQB2IQDQEf7A3uHVo4wZbbASrg0GW94EPBM14on8PBNsvo
0Okr/FC+gIP5jSUSHlsFoHwRhxVTZBjXJwlC+RIObv8ijC9+gPFlHCK/quv3
xwpB+QoON/5OOTVxNYyv5NCmvi/vwgMYX8WhNvL/4WfMwlC+msNuJaMFNVIw
voaD9Mbdruc0YHwth9Unu/+cNoTxdRzK90ZMzjCH8fUcZvxYefOpFYxv4DDV
fBODgC2Mb+jgZXffI90OxjdycFYSPmBtD+MbO/C3nNLdD+ebOKSVzi8WcIDx
TR3UJO5tcoLzzRy+Kk45lAjnmzskH9z5tQTOt3BgMzpwuQHOt3Ro0qnW7IDz
rRyM9bk29cD51g4VH0Lq++F8G4eJXCusJsL5tg6+bst9JsH5dg76Fv+/Ivj2
UJqBAQBrWm7q
      "]]}, 
    {RGBColor[0.6, 0.24, 0.4428931686004542], PointBox[CompressedData["
1:eJxTTMoPSmViYGCQB2IQDQEf7OWurY/6VyHtABVwmPnFbUnGcXEon8OB0+RB
76EXolC+gMOttH31G6+LQPkiDgUn+BUvm8H4Eg6vN6zkmHhNGMqXcbCxvbwy
aQ6Mr+Awcc72Z98qYHwlh/LHWi89k2F8FYffB5t1foXC+GoOu68XNZzwhfE1
HL6bWvru9oDxtRwCJHq95rrC+DoOjTxB/RucYXw9h/D8V6YNTjC+gUNQXb/u
EUcY39DhnN/1v/5wvpHD5/oCQw4439gh5mHdpdsOML6Jw5X6trQDcL6pw+l1
jCXr4XwzB8/OFb+WwPnmDrt2VDfOg/MtHBq+uTfOgvMtHb7tdOeYAedbOTw+
zNc4Dc63duja69w/Fc63cfgbevnnFDjf1mGVYvlcBN/O4fXbxBQE3x5KMzAA
ANxFeDM=
      "]]}}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  Method->{},
  PlotRange->{{0, 31.}, {0, 4.985582754125071}},
  PlotRangeClipping->True,
  PlotRangePadding->{{0.62, 0.62}, {0.09971165508250142, 
   0.09971165508250142}}]], "Print",
 CellChangeTimes->{{3.5718073878676753`*^9, 3.571807417617963*^9}, 
   3.5718075008322654`*^9, 3.571807533602523*^9, {3.571807586808528*^9, 
   3.5718076011293297`*^9}, 3.571808160452837*^9, {3.571808345998195*^9, 
   3.571808371661249*^9}, 3.571809381760548*^9, 3.571809519134513*^9, {
   3.5718095538122764`*^9, 3.571809582174282*^9}, 3.5718611491639004`*^9}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"iterative=\"\>", "\[InvisibleSpace]", "31", 
   "\[InvisibleSpace]", "\<\"      convergence value=\"\>", 
   "\[InvisibleSpace]", "2.000001505222313`"}],
  SequenceForm[
  "iterative=", 31, "      convergence value=", 2.000001505222313],
  Editable->False]], "Print",
 CellChangeTimes->{{3.5718073878676753`*^9, 3.571807417617963*^9}, 
   3.5718075008322654`*^9, 3.571807533602523*^9, {3.571807586808528*^9, 
   3.5718076011293297`*^9}, 3.571808160452837*^9, {3.571808345998195*^9, 
   3.571808371661249*^9}, 3.571809381760548*^9, 3.571809519134513*^9, {
   3.5718095538122764`*^9, 3.571809582174282*^9}, 3.571861149197902*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "0.4472132589221483`", ",", "0.4472132589221483`", ",", 
   "0.4472135954998363`", ",", "0.44721426865506037`", ",", 
   "0.4472135954998363`"}], "}"}]], "Print",
 CellChangeTimes->{{3.5718073878676753`*^9, 3.571807417617963*^9}, 
   3.5718075008322654`*^9, 3.571807533602523*^9, {3.571807586808528*^9, 
   3.5718076011293297`*^9}, 3.571808160452837*^9, {3.571808345998195*^9, 
   3.571808371661249*^9}, 3.571809381760548*^9, 3.571809519134513*^9, {
   3.5718095538122764`*^9, 3.571809582174282*^9}, 3.5718611492019024`*^9}],

Cell[BoxData[
 GraphicsBox[{{}, {
    {RGBColor[0.24720000000000017`, 0.24, 0.6], PointBox[CompressedData["
1:eJxTTMoPSmViYGBQBGIQDQEf7G0jDHuqDzE5QAUcysJs5vZOZoXyORxi1N6b
8b+E8QUc7Bj+rRVPZYPyRRxeSEipHL0K40s4bAyKeK6lxw7lyzi07XDKFcqH
8RUcNoYvmyo0F8ZXctjX2Xbu3x4YX8VBeWa5761LML6aw7XPJvvXP4TxNRyU
dDfs3/gaxtdyqPmgkJP9CcbXcWj8fTr07TcYX8+hTe195MefML6Bwx3nGoev
v2F8Q4eCJQK7ff/C+EYOWq4OC4z/wfjGDqw+0f/PwPkmDt2f+byN/8P4pg7B
O/81lcP5Zg42rLwMS+F8c4fAGQ0hh+F8C4f+pR691+F8S4dTUes5nsL5Vg5h
p7exvoPzrR3E+o4xf4HzbRw62huYfsD5tg7bfgWv+QXn2zm8f/Kw6A+cb++w
ePrrhX/hfAeHnXafsv/B+A0OUHEGBgChvYh2
      "]]}, 
    {RGBColor[0.6, 0.24, 0.4428931686004542], PointBox[CompressedData["
1:eJxTTMoPSmViYGBQBGIQDQEf7LPuLDilZyPiABVwOBi9alFYCg+Uz+HQ9W3P
z653nFC+gMPHyNzGs3IwvogDB9NXbY5tHFC+hMMhu31n2cthfBmHMyZv56zz
g/EVHIwd56RYm8D4Sg67N8xi3KsE46s4xL04IK0gDuOrOVidqFRK54fxNRzO
BzKbOHPB+FoODMkVp66xwfg6DodiE572scD4eg7Pe6ef4WSG8Q0cbG83VLIx
wfiGDl9W7JkfyQjjGzlwVk1If8YA4xs7hOwWnjkTzjdxqCyvTE2D800dDmxN
cXaB880c9v9TbdKF880dfnJvD5OD8y0c1OectBSB8y0djujdrOaF860cJgVa
/eWA860d9ser/WOF820c4tiO/GOG820dtr66vpIJzrdzKHnWf4wRzrd3mD6F
Sx/Bd3Co4WY/xQDjNzhAxRkYAFXYapE=
      "]]}}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  Method->{},
  PlotRange->{{0, 33.}, {0, 3.045594318446867}},
  PlotRangeClipping->True,
  PlotRangePadding->{{0.66, 0.66}, {0.060911886368937344`, 
   0.060911886368937344`}}]], "Print",
 CellChangeTimes->{{3.5718073878676753`*^9, 3.571807417617963*^9}, 
   3.5718075008322654`*^9, 3.571807533602523*^9, {3.571807586808528*^9, 
   3.5718076011293297`*^9}, 3.571808160452837*^9, {3.571808345998195*^9, 
   3.571808371661249*^9}, 3.571809381760548*^9, 3.571809519134513*^9, {
   3.5718095538122764`*^9, 3.571809582174282*^9}, 3.5718611492329044`*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"0.0312002000000000008`4.514757291228131", " ", 
      SuperscriptBox["Null", "3"]}], ",", 
     SuperscriptBox["Null", "4"]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"0.046800300000000003`4.690848550283811", " ", 
      SuperscriptBox["Null", "3"]}], ",", 
     SuperscriptBox["Null", "4"]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"0.046800300000000003`4.690848550283811", " ", 
      SuperscriptBox["Null", "3"]}], ",", 
     SuperscriptBox["Null", "4"]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.5718073322917376`*^9, 3.571807340972825*^9}, {
   3.571807383609233*^9, 3.571807417660965*^9}, 3.571807501605305*^9, 
   3.5718075342819586`*^9, {3.5718075875367675`*^9, 3.57180760183237*^9}, 
   3.5718081606592455`*^9, {3.5718083462018013`*^9, 3.5718083718632607`*^9}, 
   3.571809382417982*^9, 3.571809519460531*^9, {3.5718095541384935`*^9, 
   3.5718095825030985`*^9}, 3.571861149245905*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"a", "=", "1.872128645746563`"}], "\[IndentingNewLine]", 
 RowBox[{"b", "=", "3.8130660710116713`"}], "\[IndentingNewLine]", 
 RowBox[{"c", "=", 
  "2.000001505222313`"}], "\[IndentingNewLine]", "degree"}], "Input",
 CellChangeTimes->{{3.5718612011357994`*^9, 3.5718612915097227`*^9}}],

Cell[BoxData["1.872128645746563`"], "Output",
 CellChangeTimes->{{3.5718612723274403`*^9, 3.571861292057754*^9}}],

Cell[BoxData["3.8130660710116713`"], "Output",
 CellChangeTimes->{{3.5718612723274403`*^9, 3.571861292060754*^9}}],

Cell[BoxData["2.000001505222313`"], "Output",
 CellChangeTimes->{{3.5718612723274403`*^9, 3.571861292062754*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"2", ",", "2", ",", "2", ",", "1", ",", "2"}], "}"}]], "Output",
 CellChangeTimes->{{3.5718612723274403`*^9, 3.5718612920647545`*^9}}]
}, Open  ]]
},
WindowSize->{1424, 805},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"9.0 for Microsoft Windows (32-bit) (2013\:5e742\:670813\
\:65e5)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 377, 9, 31, "Input"],
Cell[937, 31, 1691, 54, 72, "Input"],
Cell[2631, 87, 3115, 84, 152, "Input"],
Cell[5749, 173, 2291, 63, 152, "Input"],
Cell[8043, 238, 694, 15, 72, "Input"],
Cell[8740, 255, 6548, 167, 296, "Input"],
Cell[CellGroupData[{
Cell[15313, 426, 388, 8, 31, "Input"],
Cell[CellGroupData[{
Cell[15726, 438, 676, 12, 23, "Print"],
Cell[16405, 452, 567, 10, 23, "Print"],
Cell[16975, 464, 1503, 31, 239, "Print"],
Cell[18481, 497, 681, 12, 23, "Print"],
Cell[19165, 511, 562, 10, 23, "Print"],
Cell[19730, 523, 1761, 35, 243, "Print"],
Cell[21494, 560, 676, 12, 23, "Print"],
Cell[22173, 574, 566, 10, 23, "Print"],
Cell[22742, 586, 1813, 35, 236, "Print"]
}, Open  ]],
Cell[24570, 624, 1020, 23, 33, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[25627, 652, 309, 5, 92, "Input"],
Cell[25939, 659, 113, 1, 31, "Output"],
Cell[26055, 662, 114, 1, 31, "Output"],
Cell[26172, 665, 113, 1, 31, "Output"],
Cell[26288, 668, 173, 3, 87, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
