(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     36634,       1049]
NotebookOptionsPosition[     35515,       1009]
NotebookOutlinePosition[     35869,       1025]
CellTagsIndexPosition[     35826,       1022]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"{", 
    RowBox[{"orderA", ",", "dimensionA"}], "}"}], "=", 
   RowBox[{"{", 
    RowBox[{"2", ",", "2"}], "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"{", 
    RowBox[{"orderB", ",", "dimensionB"}], "}"}], "=", 
   RowBox[{"{", 
    RowBox[{"3", ",", "3"}], "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"TenM", "=", 
   RowBox[{"orderA", "+", "orderB"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"TenN", "=", 
   RowBox[{"dimensionA", "+", "dimensionB"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.573561020032238*^9, 3.5735610203598385`*^9}, {
  3.5735613367654276`*^9, 3.573561344986642*^9}, {3.573561422518778*^9, 
  3.5735614232831793`*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"fA", "[", 
   RowBox[{"A_", ",", "x_", ",", "y_"}], "]"}], ":=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"Sum", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"A", "[", 
         RowBox[{"[", 
          RowBox[{"i", ",", "i2", ",", "j1", ",", "j2", ",", "j3"}], "]"}], 
         "]"}], 
        RowBox[{"x", "[", 
         RowBox[{"[", "i2", "]"}], "]"}], 
        RowBox[{"y", "[", 
         RowBox[{"[", "j1", "]"}], "]"}], 
        RowBox[{"y", "[", 
         RowBox[{"[", "j2", "]"}], "]"}], 
        RowBox[{"y", "[", 
         RowBox[{"[", "j3", "]"}], "]"}]}], ",", 
       RowBox[{"{", 
        RowBox[{"i2", ",", "1", ",", "dimensionA"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"j1", ",", "1", ",", "dimensionB"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"j2", ",", "1", ",", "dimensionB"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"j3", ",", "1", ",", "dimensionB"}], "}"}]}], "]"}], "+", 
     RowBox[{
      RowBox[{"x", "[", 
       RowBox[{"[", "i", "]"}], "]"}], "^", 
      RowBox[{"(", 
       RowBox[{"TenM", "-", "1"}], ")"}]}]}], ",", 
    RowBox[{"{", 
     RowBox[{"i", ",", "1", ",", "dimensionA"}], "}"}]}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"fB", "[", 
   RowBox[{"A_", ",", "x_", ",", "y_"}], "]"}], ":=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"Sum", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"A", "[", 
         RowBox[{"[", 
          RowBox[{"i1", ",", "i2", ",", "j", ",", "j2", ",", "j3"}], "]"}], 
         "]"}], 
        RowBox[{"x", "[", 
         RowBox[{"[", "i1", "]"}], "]"}], 
        RowBox[{"x", "[", 
         RowBox[{"[", "i2", "]"}], "]"}], 
        RowBox[{"y", "[", 
         RowBox[{"[", "j2", "]"}], "]"}], 
        RowBox[{"y", "[", 
         RowBox[{"[", "j3", "]"}], "]"}]}], ",", 
       RowBox[{"{", 
        RowBox[{"i1", ",", "1", ",", "dimensionA"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"i2", ",", "1", ",", "dimensionA"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"j2", ",", "1", ",", "dimensionB"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"j3", ",", "1", ",", "dimensionB"}], "}"}]}], "]"}], "+", 
     RowBox[{
      RowBox[{"y", "[", 
       RowBox[{"[", "j", "]"}], "]"}], "^", 
      RowBox[{"(", 
       RowBox[{"TenM", "-", "1"}], ")"}]}]}], ",", 
    RowBox[{"{", 
     RowBox[{"j", ",", "1", ",", "dimensionB"}], "}"}]}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"gA1", "[", "x_", "]"}], ":=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"x", "[", 
      RowBox[{"[", "i", "]"}], "]"}], "^", 
     RowBox[{"(", 
      RowBox[{"1", "/", 
       RowBox[{"(", 
        RowBox[{"TenM", "-", "1"}], ")"}]}], ")"}]}], ",", 
    RowBox[{"{", 
     RowBox[{"i", ",", "1", ",", "dimensionA"}], "}"}]}], "]"}]}], "\n", 
 RowBox[{
  RowBox[{"gB1", "[", "y_", "]"}], ":=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"y", "[", 
      RowBox[{"[", "i", "]"}], "]"}], "^", 
     RowBox[{"(", 
      RowBox[{"1", "/", 
       RowBox[{"(", 
        RowBox[{"TenM", "-", "1"}], ")"}]}], ")"}]}], ",", 
    RowBox[{"{", 
     RowBox[{"i", ",", "1", ",", "dimensionB"}], "}"}]}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"gA2", "[", "x_", "]"}], ":=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"x", "[", 
      RowBox[{"[", "i", "]"}], "]"}], "^", 
     RowBox[{"(", 
      RowBox[{"TenM", "-", "1"}], ")"}]}], ",", 
    RowBox[{"{", 
     RowBox[{"i", ",", "1", ",", "dimensionA"}], "}"}]}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"gB2", "[", "y_", "]"}], ":=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"y", "[", 
      RowBox[{"[", "i", "]"}], "]"}], "^", 
     RowBox[{"(", 
      RowBox[{"TenM", "-", "1"}], ")"}]}], ",", 
    RowBox[{"{", 
     RowBox[{"i", ",", "1", ",", "dimensionB"}], "}"}]}], "]"}]}]}], "Input",
 CellChangeTimes->{{3.573560738607744*^9, 3.5735607763910103`*^9}, 
   3.573560862222361*^9, {3.573561001000205*^9, 3.5735610508266926`*^9}, {
   3.573561082494748*^9, 3.573561095739171*^9}, {3.573561738372533*^9, 
   3.5735617438481426`*^9}, {3.5735623775680556`*^9, 3.5735623786912575`*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"ind", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"1", ",", "2", ",", "3", ",", "1", ",", "2"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1", ",", "2", ",", "1", ",", "3", ",", "2"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"2", ",", "1", ",", "2", ",", "1", ",", "3"}], "}"}]}], "}"}]}],
   ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"list", "=", 
   RowBox[{"Array", "[", 
    RowBox[{
     RowBox[{"TenN", "&"}], ",", 
     RowBox[{"{", "TenM", "}"}]}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"tensor", "=", 
   RowBox[{"Array", "[", 
    RowBox[{
     RowBox[{"0", "&"}], ",", "list"}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"rule", "=", 
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"ind", "[", 
       RowBox[{"[", "i", "]"}], "]"}], "\[Rule]", "i"}], ",", 
     RowBox[{"{", 
      RowBox[{"i", ",", 
       RowBox[{"Length", "[", "ind", "]"}]}], "}"}]}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"tensor", "=", 
   RowBox[{"ReplacePart", "[", 
    RowBox[{"tensor", ",", "rule"}], "]"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.573560658220803*^9, 3.5735607019632797`*^9}, {
  3.573561330151016*^9, 3.573561367903082*^9}, {3.5735614347335997`*^9, 
  3.573561447962423*^9}, {3.5735617936122303`*^9, 3.5735618247030845`*^9}, {
  3.573562065068307*^9, 3.573562069607915*^9}, {3.573562230678198*^9, 
  3.5735622322226005`*^9}, {3.5735623831840653`*^9, 3.5735623833244658`*^9}, {
  3.5735646467606525`*^9, 3.5735646471194534`*^9}, {3.573564851183412*^9, 
  3.573564851339412*^9}}],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"x", " ", "=", " ", 
   RowBox[{"Array", "[", 
    RowBox[{
     RowBox[{"#", "&"}], ",", " ", "dimensionA"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"y", "=", 
   RowBox[{"Array", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"dimensionB", "+", "1", "-", "#"}], "&"}], ",", "dimensionB"}], 
    "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"t", "=", 
   RowBox[{"{", 
    RowBox[{"2", ",", "1"}], "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Timing", "[", 
  RowBox[{"For", "[", 
   RowBox[{
    RowBox[{"k", "=", "0"}], ",", 
    RowBox[{
     RowBox[{
      RowBox[{"Abs", "[", 
       RowBox[{
        RowBox[{"t", "[", 
         RowBox[{"[", "1", "]"}], "]"}], "-", 
        RowBox[{"t", "[", 
         RowBox[{"[", "2", "]"}], "]"}]}], "]"}], "\[GreaterEqual]", 
      RowBox[{"10", "^", 
       RowBox[{"-", "6"}]}]}], "&&", 
     RowBox[{"k", "<", "3000"}]}], ",", 
    RowBox[{"k", "++"}], ",", "\[IndentingNewLine]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"z1", "=", 
       RowBox[{"fA", "[", 
        RowBox[{"tensor", ",", "x", ",", "y"}], "]"}]}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{"z2", "=", 
       RowBox[{"fB", "[", 
        RowBox[{"tensor", ",", "x", ",", "y"}], "]"}]}], ",", "\n", "       ", 
      RowBox[{"x", "=", 
       RowBox[{
        RowBox[{
         RowBox[{"gA1", "[", "z1", "]"}], "/", 
         RowBox[{"Norm", "[", 
          RowBox[{"Join", "[", 
           RowBox[{
            RowBox[{"gA1", "[", "z1", "]"}], ",", 
            RowBox[{"gB1", "[", "z2", "]"}]}], "]"}], "]"}]}], "//", "N"}]}], 
      ",", "\[IndentingNewLine]", 
      RowBox[{"y", "=", 
       RowBox[{
        RowBox[{
         RowBox[{"gB1", "[", "z2", "]"}], "/", 
         RowBox[{"Norm", "[", 
          RowBox[{"Join", "[", 
           RowBox[{
            RowBox[{"gA1", "[", "z1", "]"}], ",", 
            RowBox[{"gB1", "[", "z2", "]"}]}], "]"}], "]"}]}], "//", "N"}]}], 
      ",", "\n", "       ", 
      RowBox[{"z1", "=", 
       RowBox[{"fA", "[", 
        RowBox[{"tensor", ",", "x", ",", "y"}], "]"}]}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{"z2", "=", 
       RowBox[{"fB", "[", 
        RowBox[{"tensor", ",", "x", ",", "y"}], "]"}]}], ",", "\n", 
      RowBox[{"t", "=", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{
          RowBox[{"Min", "[", 
           RowBox[{"Join", "[", 
            RowBox[{
             RowBox[{"Table", "[", 
              RowBox[{
               RowBox[{"If", "[", 
                RowBox[{
                 RowBox[{
                  RowBox[{
                   RowBox[{"gA2", "[", "x", "]"}], "[", 
                   RowBox[{"[", "i", "]"}], "]"}], ">", "0"}], ",", 
                 RowBox[{
                  RowBox[{"z1", "[", 
                   RowBox[{"[", "i", "]"}], "]"}], "/", 
                  RowBox[{
                   RowBox[{"gA2", "[", "x", "]"}], "[", 
                   RowBox[{"[", "i", "]"}], "]"}]}], ",", "Infinity"}], "]"}],
                ",", 
               RowBox[{"{", 
                RowBox[{"i", ",", "1", ",", "dimensionA"}], "}"}]}], "]"}], 
             ",", 
             RowBox[{"Table", "[", 
              RowBox[{
               RowBox[{"If", "[", 
                RowBox[{
                 RowBox[{
                  RowBox[{
                   RowBox[{"gB2", "[", "y", "]"}], "[", 
                   RowBox[{"[", "j", "]"}], "]"}], ">", "0"}], ",", 
                 RowBox[{
                  RowBox[{"z2", "[", 
                   RowBox[{"[", "j", "]"}], "]"}], "/", 
                  RowBox[{
                   RowBox[{"gB2", "[", "y", "]"}], "[", 
                   RowBox[{"[", "j", "]"}], "]"}]}], ",", "Infinity"}], "]"}],
                ",", 
               RowBox[{"{", 
                RowBox[{"j", ",", "1", ",", "dimensionB"}], "}"}]}], "]"}]}], 
            "]"}], "]"}], ",", 
          RowBox[{"Max", "[", 
           RowBox[{"Join", "[", 
            RowBox[{
             RowBox[{"Table", "[", 
              RowBox[{
               RowBox[{"If", "[", 
                RowBox[{
                 RowBox[{
                  RowBox[{
                   RowBox[{"gA2", "[", "x", "]"}], "[", 
                   RowBox[{"[", "i", "]"}], "]"}], ">", "0"}], ",", 
                 RowBox[{
                  RowBox[{"z1", "[", 
                   RowBox[{"[", "i", "]"}], "]"}], "/", 
                  RowBox[{
                   RowBox[{"gA2", "[", "x", "]"}], "[", 
                   RowBox[{"[", "i", "]"}], "]"}]}], ",", 
                 RowBox[{"-", "Infinity"}]}], "]"}], ",", 
               RowBox[{"{", 
                RowBox[{"i", ",", "1", ",", "dimensionA"}], "}"}]}], "]"}], 
             ",", 
             RowBox[{"Table", "[", 
              RowBox[{
               RowBox[{"If", "[", 
                RowBox[{
                 RowBox[{
                  RowBox[{
                   RowBox[{"gB2", "[", "y", "]"}], "[", 
                   RowBox[{"[", "j", "]"}], "]"}], ">", "0"}], ",", 
                 RowBox[{
                  RowBox[{"z2", "[", 
                   RowBox[{"[", "j", "]"}], "]"}], "/", 
                  RowBox[{
                   RowBox[{"gB2", "[", "y", "]"}], "[", 
                   RowBox[{"[", "j", "]"}], "]"}]}], ",", 
                 RowBox[{"-", "Infinity"}]}], "]"}], ",", 
               RowBox[{"{", 
                RowBox[{"j", ",", "1", ",", "dimensionB"}], "}"}]}], "]"}]}], 
            "]"}], "]"}]}], "}"}], "//", "N"}]}]}], "}"}]}], "]"}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{"Print", "[", 
  RowBox[{"\"\<iterative=\>\"", ",", 
   RowBox[{"k", "-", "1"}], ",", "\"\<  convergence value=\>\"", ",", 
   RowBox[{
    RowBox[{"t", "[", 
     RowBox[{"[", "2", "]"}], "]"}], "-", "1"}], ",", 
   "\"\<  convergence vector x=\>\"", ",", 
   RowBox[{"Chop", "[", "x", "]"}], ",", "\"\<  convergence vector y=\>\"", 
   ",", 
   RowBox[{"Chop", "[", "y", "]"}], ",", "\"\<   Norm Error x=\>\"", ",", 
   RowBox[{"Norm", "[", 
    RowBox[{
     RowBox[{"fA", "[", 
      RowBox[{"tensor", ",", "x", ",", "y"}], "]"}], "-", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"t", "[", 
        RowBox[{"[", "2", "]"}], "]"}], ")"}], 
      RowBox[{"gA2", "[", "x", "]"}]}]}], "]"}], ",", 
   "\"\<   Norm Error y=\>\"", ",", 
   RowBox[{"Norm", "[", 
    RowBox[{
     RowBox[{"fB", "[", 
      RowBox[{"tensor", ",", "x", ",", "y"}], "]"}], "-", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"t", "[", 
        RowBox[{"[", "2", "]"}], "]"}], ")"}], 
      RowBox[{"gB2", "[", "y", "]"}]}]}], "]"}]}], "]"}]}], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0.046800300000000003`4.690848550283811", ",", "Null"}], 
  "}"}]], "Output",
 CellChangeTimes->{3.5735624077385087`*^9, 3.573563019209794*^9, 
  3.573563991044701*^9, 3.5735641752654247`*^9, 3.573564521305232*^9, 
  3.5735648542566175`*^9, 3.573565558270254*^9}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"iterative=\"\>", "\[InvisibleSpace]", "9", 
   "\[InvisibleSpace]", "\<\"  convergence value=\"\>", "\[InvisibleSpace]", 
   "2.2206430720209136`", 
   "\[InvisibleSpace]", "\<\"  convergence vector x=\"\>", 
   "\[InvisibleSpace]", 
   RowBox[{"{", 
    RowBox[{"0.4716835757860336`", ",", "0.47168357571170305`"}], "}"}], 
   "\[InvisibleSpace]", "\<\"  convergence vector y=\"\>", 
   "\[InvisibleSpace]", 
   RowBox[{"{", 
    RowBox[{
    "0.43494316390503535`", ",", "0.47168357571170305`", ",", 
     "0.3786400101859586`"}], "}"}], 
   "\[InvisibleSpace]", "\<\"   Norm Error x=\"\>", "\[InvisibleSpace]", 
   "1.5118870060200152`*^-10", 
   "\[InvisibleSpace]", "\<\"   Norm Error y=\"\>", "\[InvisibleSpace]", 
   "6.457740982175346`*^-9"}],
  SequenceForm[
  "iterative=", 9, "  convergence value=", 2.2206430720209136`, 
   "  convergence vector x=", {0.4716835757860336, 0.47168357571170305`}, 
   "  convergence vector y=", {0.43494316390503535`, 0.47168357571170305`, 
   0.3786400101859586}, "   Norm Error x=", 1.5118870060200152`*^-10, 
   "   Norm Error y=", 6.457740982175346*^-9],
  Editable->False]], "Print",
 CellChangeTimes->{3.5735624077697086`*^9, 3.573563019209794*^9, 
  3.573563991075901*^9, 3.5735641752654247`*^9, 3.573564521305232*^9, 
  3.5735648542722178`*^9, 3.573565558270254*^9}]
}, Open  ]],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"tensorx", "[", 
    RowBox[{"A_", ",", "y_"}], "]"}], ":=", 
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{"Sum", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"A", "[", 
         RowBox[{"[", 
          RowBox[{"i1", ",", "i2", ",", "j1", ",", "j2", ",", "j3"}], "]"}], 
         "]"}], 
        RowBox[{"y", "[", 
         RowBox[{"[", "j1", "]"}], "]"}], 
        RowBox[{"y", "[", 
         RowBox[{"[", "j2", "]"}], "]"}], 
        RowBox[{"y", "[", 
         RowBox[{"[", "j3", "]"}], "]"}]}], ",", 
       RowBox[{"{", 
        RowBox[{"j1", ",", "dimensionB"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"j2", ",", "dimensionB"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"j3", ",", "dimensionB"}], "}"}]}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"i1", ",", "dimensionA"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"i2", ",", "dimensionA"}], "}"}]}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"fx", "[", 
   RowBox[{"Ay_", ",", "x_"}], "]"}], ":=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{"Sum", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"Ay", "[", 
        RowBox[{"[", 
         RowBox[{"i", ",", "i2"}], "]"}], "]"}], 
       RowBox[{"x", "[", 
        RowBox[{"[", "i2", "]"}], "]"}]}], ",", 
      RowBox[{"{", 
       RowBox[{"i2", ",", "1", ",", "dimensionA"}], "}"}]}], "]"}], 
    RowBox[{"(*", 
     RowBox[{"+", 
      RowBox[{
       RowBox[{"x", "[", 
        RowBox[{"[", "i", "]"}], "]"}], "^", 
       RowBox[{"(", 
        RowBox[{"TenM", "-", "1"}], ")"}]}]}], "*)"}], ",", 
    RowBox[{"{", 
     RowBox[{"i", ",", "1", ",", "dimensionA"}], "}"}]}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"gx1", "[", "x_", "]"}], ":=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"x", "[", 
      RowBox[{"[", "i", "]"}], "]"}], "^", 
     RowBox[{"(", 
      RowBox[{"1", "/", 
       RowBox[{"(", 
        RowBox[{"TenM", "-", "1"}], ")"}]}], ")"}]}], ",", 
    RowBox[{"{", 
     RowBox[{"i", ",", "1", ",", "dimensionA"}], "}"}]}], "]"}]}], "\n", 
 RowBox[{
  RowBox[{"gx2", "[", "x_", "]"}], ":=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"x", "[", 
      RowBox[{"[", "i", "]"}], "]"}], "^", 
     RowBox[{"(", 
      RowBox[{"TenM", "-", "1"}], ")"}]}], ",", 
    RowBox[{"{", 
     RowBox[{"i", ",", "1", ",", "dimensionA"}], "}"}]}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"tensory", "[", 
    RowBox[{"A_", ",", "x_"}], "]"}], ":=", 
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{"Sum", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"A", "[", 
         RowBox[{"[", 
          RowBox[{"i1", ",", "i2", ",", "j1", ",", "j2", ",", "j3"}], "]"}], 
         "]"}], 
        RowBox[{"x", "[", 
         RowBox[{"[", "i1", "]"}], "]"}], 
        RowBox[{"x", "[", 
         RowBox[{"[", "i2", "]"}], "]"}]}], ",", 
       RowBox[{"{", 
        RowBox[{"i1", ",", "dimensionA"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"i2", ",", "dimensionA"}], "}"}]}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"j1", ",", "dimensionB"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"j2", ",", "dimensionB"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"j3", ",", "dimensionB"}], "}"}]}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"fy", "[", 
   RowBox[{"Ax_", ",", "y_"}], "]"}], ":=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{"Sum", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"Ax", "[", 
        RowBox[{"[", 
         RowBox[{"j", ",", "j2", ",", "j3"}], "]"}], "]"}], 
       RowBox[{"y", "[", 
        RowBox[{"[", "j2", "]"}], "]"}], 
       RowBox[{"y", "[", 
        RowBox[{"[", "j3", "]"}], "]"}]}], ",", 
      RowBox[{"{", 
       RowBox[{"j2", ",", "1", ",", "dimensionB"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"j3", ",", "1", ",", "dimensionB"}], "}"}]}], "]"}], 
    RowBox[{"(*", 
     RowBox[{"+", 
      RowBox[{
       RowBox[{"y", "[", 
        RowBox[{"[", "j", "]"}], "]"}], "^", 
       RowBox[{"(", 
        RowBox[{"TenM", "-", "1"}], ")"}]}]}], "*)"}], ",", 
    RowBox[{"{", 
     RowBox[{"j", ",", "1", ",", "dimensionB"}], "}"}]}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"gy1", "[", "y_", "]"}], ":=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"y", "[", 
      RowBox[{"[", "j", "]"}], "]"}], "^", 
     RowBox[{"(", 
      RowBox[{"1", "/", 
       RowBox[{"(", 
        RowBox[{"TenM", "-", "1"}], ")"}]}], ")"}]}], ",", 
    RowBox[{"{", 
     RowBox[{"j", ",", "1", ",", "dimensionB"}], "}"}]}], "]"}]}], "\n", 
 RowBox[{
  RowBox[{"gy2", "[", "y_", "]"}], ":=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"y", "[", 
      RowBox[{"[", "j", "]"}], "]"}], "^", 
     RowBox[{"(", 
      RowBox[{"TenM", "-", "1"}], ")"}]}], ",", 
    RowBox[{"{", 
     RowBox[{"j", ",", "1", ",", "dimensionB"}], "}"}]}], "]"}]}]}], "Input",
 CellChangeTimes->{{3.573561133959238*^9, 3.5735611496060658`*^9}, {
   3.573561182163323*^9, 3.573561186968131*^9}, {3.57356127559772*^9, 
   3.5735612813229303`*^9}, {3.5735614810032806`*^9, 3.5735615302681675`*^9}, 
   3.573561567489833*^9, {3.573562523163111*^9, 3.5735626558257446`*^9}, {
   3.573562692017808*^9, 3.5735627649947357`*^9}, {3.573563146568418*^9, 
   3.573563148877222*^9}, {3.5735649591356015`*^9, 3.573564965703213*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"x", " ", "=", " ", 
   RowBox[{"Array", "[", 
    RowBox[{
     RowBox[{"#", "&"}], ",", " ", "dimensionA"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"y", "=", 
   RowBox[{"Array", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"dimensionB", "+", "1", "-", "#"}], "&"}], ",", "dimensionB"}], 
    "]"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.5735628283308473`*^9, 3.5735628304992514`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"eigx", "[", "y_", "]"}], ":=", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"For", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{"t", "=", 
          RowBox[{"{", 
           RowBox[{"2", ",", "1"}], "}"}]}], ",", 
         RowBox[{"tensortest", "=", 
          RowBox[{"tensorx", "[", 
           RowBox[{"tensor", ",", "y"}], "]"}]}], ",", 
         RowBox[{"k", "=", "0"}]}], "}"}], ",", 
       RowBox[{
        RowBox[{
         RowBox[{"Abs", "[", 
          RowBox[{
           RowBox[{"t", "[", 
            RowBox[{"[", "1", "]"}], "]"}], "-", 
           RowBox[{"t", "[", 
            RowBox[{"[", "2", "]"}], "]"}]}], "]"}], "\[GreaterEqual]", 
         RowBox[{"10", "^", 
          RowBox[{"-", "6"}]}]}], "&&", 
        RowBox[{"k", "\[LessEqual]", "3000"}]}], ",", 
       RowBox[{"k", "++"}], ",", 
       RowBox[{"{", "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"funx", "=", 
          RowBox[{"fx", "[", 
           RowBox[{"tensortest", ",", "x"}], "]"}]}], ",", "\n", 
         RowBox[{"x", "=", 
          RowBox[{
           RowBox[{
            RowBox[{"gx1", "[", "funx", "]"}], "/", 
            RowBox[{"Norm", "[", 
             RowBox[{"gx1", "[", "funx", "]"}], "]"}]}], "//", "N"}]}], ",", 
         "\n", 
         RowBox[{"funx", "=", 
          RowBox[{
           RowBox[{"fx", "[", 
            RowBox[{"tensortest", ",", "x"}], "]"}], "//", "N"}]}], ",", "\n", 
         RowBox[{"t", "=", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{
             RowBox[{"Min", "[", 
              RowBox[{"Table", "[", 
               RowBox[{
                RowBox[{"If", "[", 
                 RowBox[{
                  RowBox[{
                   RowBox[{
                    RowBox[{"gx2", "[", "x", "]"}], "[", 
                    RowBox[{"[", "i", "]"}], "]"}], ">", "0"}], ",", 
                  RowBox[{
                   RowBox[{"funx", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], "/", 
                   RowBox[{
                    RowBox[{"gx2", "[", "x", "]"}], "[", 
                    RowBox[{"[", "i", "]"}], "]"}]}], ",", "Infinity"}], 
                 "]"}], ",", 
                RowBox[{"{", 
                 RowBox[{"i", ",", "1", ",", "dimensionA"}], "}"}]}], "]"}], 
              "]"}], ",", 
             RowBox[{"Max", "[", 
              RowBox[{"Table", "[", 
               RowBox[{
                RowBox[{"If", "[", 
                 RowBox[{
                  RowBox[{
                   RowBox[{
                    RowBox[{"gx2", "[", "x", "]"}], "[", 
                    RowBox[{"[", "i", "]"}], "]"}], ">", "0"}], ",", 
                  RowBox[{
                   RowBox[{"funx", "[", 
                    RowBox[{"[", "i", "]"}], "]"}], "/", 
                   RowBox[{
                    RowBox[{"gx2", "[", "x", "]"}], "[", 
                    RowBox[{"[", "i", "]"}], "]"}]}], ",", 
                  RowBox[{"-", "Infinity"}]}], "]"}], ",", 
                RowBox[{"{", 
                 RowBox[{"i", ",", "1", ",", "dimensionA"}], "}"}]}], "]"}], 
              "]"}]}], "}"}], "//", "N"}]}]}], "}"}]}], "]"}], ";", 
     RowBox[{"{", 
      RowBox[{"x", ",", 
       RowBox[{"t", "[", 
        RowBox[{"[", "2", "]"}], "]"}]}], "}"}]}], "}"}], "//", 
   "Flatten"}]}]], "Input",
 CellChangeTimes->{{3.573564113926117*^9, 3.573564205435878*^9}, {
  3.5735645112432146`*^9, 3.573564512210417*^9}, {3.573564581380938*^9, 
  3.5735646176978016`*^9}, {3.573564835692585*^9, 3.5735648384537897`*^9}, {
  3.573564917842329*^9, 3.573564946483979*^9}, {3.573564983877245*^9, 
  3.573564994266863*^9}, {3.573565046199354*^9, 3.5735650803634143`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"eigy", "[", "x_", "]"}], ":=", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"For", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{"t", "=", 
          RowBox[{"{", 
           RowBox[{"2", ",", "1"}], "}"}]}], ",", 
         RowBox[{"tensortest", "=", 
          RowBox[{"tensory", "[", 
           RowBox[{"tensor", ",", "x"}], "]"}]}], ",", 
         RowBox[{"k", "=", "0"}]}], "}"}], ",", 
       RowBox[{
        RowBox[{
         RowBox[{"Abs", "[", 
          RowBox[{
           RowBox[{"t", "[", 
            RowBox[{"[", "1", "]"}], "]"}], "-", 
           RowBox[{"t", "[", 
            RowBox[{"[", "2", "]"}], "]"}]}], "]"}], "\[GreaterEqual]", 
         RowBox[{"10", "^", 
          RowBox[{"-", "6"}]}]}], "&&", 
        RowBox[{"k", "\[LessEqual]", "3000"}]}], ",", 
       RowBox[{"k", "++"}], ",", 
       RowBox[{"{", "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"funy", "=", 
          RowBox[{"fy", "[", 
           RowBox[{"tensortest", ",", "y"}], "]"}]}], ",", "\n", 
         RowBox[{"y", "=", 
          RowBox[{
           RowBox[{
            RowBox[{"gy1", "[", "funy", "]"}], "/", 
            RowBox[{"Norm", "[", 
             RowBox[{"gy1", "[", "funy", "]"}], "]"}]}], "//", "N"}]}], ",", 
         "\n", 
         RowBox[{"funy", "=", 
          RowBox[{
           RowBox[{"fy", "[", 
            RowBox[{"tensortest", ",", "y"}], "]"}], "//", "N"}]}], ",", "\n", 
         RowBox[{"t", "=", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{
             RowBox[{"Min", "[", 
              RowBox[{"Table", "[", 
               RowBox[{
                RowBox[{"If", "[", 
                 RowBox[{
                  RowBox[{
                   RowBox[{
                    RowBox[{"gy2", "[", "y", "]"}], "[", 
                    RowBox[{"[", "j", "]"}], "]"}], ">", "0"}], ",", 
                  RowBox[{
                   RowBox[{"funy", "[", 
                    RowBox[{"[", "j", "]"}], "]"}], "/", 
                   RowBox[{
                    RowBox[{"gy2", "[", "y", "]"}], "[", 
                    RowBox[{"[", "j", "]"}], "]"}]}], ",", "Infinity"}], 
                 "]"}], ",", 
                RowBox[{"{", 
                 RowBox[{"j", ",", "1", ",", "dimensionB"}], "}"}]}], "]"}], 
              "]"}], ",", 
             RowBox[{"Max", "[", 
              RowBox[{"Table", "[", 
               RowBox[{
                RowBox[{"If", "[", 
                 RowBox[{
                  RowBox[{
                   RowBox[{
                    RowBox[{"gy2", "[", "y", "]"}], "[", 
                    RowBox[{"[", "j", "]"}], "]"}], ">", "0"}], ",", 
                  RowBox[{
                   RowBox[{"funy", "[", 
                    RowBox[{"[", "j", "]"}], "]"}], "/", 
                   RowBox[{
                    RowBox[{"gy2", "[", "y", "]"}], "[", 
                    RowBox[{"[", "j", "]"}], "]"}]}], ",", 
                  RowBox[{"-", "Infinity"}]}], "]"}], ",", 
                RowBox[{"{", 
                 RowBox[{"j", ",", "1", ",", "dimensionB"}], "}"}]}], "]"}], 
              "]"}]}], "}"}], "//", "N"}]}]}], "}"}]}], "]"}], ";", 
     RowBox[{"{", 
      RowBox[{"y", ",", 
       RowBox[{"t", "[", 
        RowBox[{"[", "2", "]"}], "]"}]}], "}"}]}], "}"}], "//", 
   "Flatten"}]}]], "Input",
 CellChangeTimes->{{3.573563419756098*^9, 3.573563538737507*^9}, {
  3.573564739643216*^9, 3.573564830731776*^9}, {3.5735650009124746`*^9, 
  3.5735650054520826`*^9}, {3.5735651066962605`*^9, 3.573565131796705*^9}}],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"tensorx", "[", 
  RowBox[{"tensor", ",", "y"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"eigx", "[", "y", "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"x", "=", 
   RowBox[{"%", "[", 
    RowBox[{"[", 
     RowBox[{"1", ";;", "dimensionA"}], "]"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"tensory", "[", 
  RowBox[{"tensor", ",", "x"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"eigy", "[", "x", "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"y", "=", 
   RowBox[{"%", "[", 
    RowBox[{"[", 
     RowBox[{"1", ";;", "dimensionB"}], "]"}], "]"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.5735659573969545`*^9, 3.5735660260370755`*^9}, {
   3.5735660702787533`*^9, 3.573566070543954*^9}, {3.573566122570045*^9, 
   3.5735661266728525`*^9}, 3.573566184034153*^9, 3.5735662303506346`*^9, {
   3.573566263999893*^9, 3.5735663352764187`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"0.`", ",", "0.5635821900435308`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0.5635821900435308`", ",", "0.`"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.573566065520745*^9, 3.57356608556678*^9}, {
   3.5735661271876535`*^9, 3.573566129496457*^9}, 3.5735661858437557`*^9, {
   3.5735662309278355`*^9, 3.573566253079874*^9}, 3.5735662910347404`*^9, {
   3.573566325729202*^9, 3.5735663681612763`*^9}, {3.5735665454711876`*^9, 
   3.573566570056831*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "0.7071067814147817`", ",", "0.7071067809583135`", ",", 
   "1.594051155915558`"}], "}"}]], "Output",
 CellChangeTimes->{{3.573566065520745*^9, 3.57356608556678*^9}, {
   3.5735661271876535`*^9, 3.573566129496457*^9}, 3.5735661858437557`*^9, {
   3.5735662309278355`*^9, 3.573566253079874*^9}, 3.5735662910347404`*^9, {
   3.573566325729202*^9, 3.5735663681612763`*^9}, {3.5735665454711876`*^9, 
   3.573566570072431*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0.`", ",", "0.`", ",", "0.`"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.`", ",", "0.`", ",", "0.`"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.`", ",", "1.0000000000000002`", ",", "0.`"}], "}"}]}], "}"}],
    ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0.`", ",", "0.`", ",", "1.5000000000000002`"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.`", ",", "0.`", ",", "0.`"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.`", ",", "0.`", ",", "0.`"}], "}"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0.`", ",", "0.5000000000000001`", ",", "0.`"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.`", ",", "0.`", ",", "0.`"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.`", ",", "0.`", ",", "0.`"}], "}"}]}], "}"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{{3.573566065520745*^9, 3.57356608556678*^9}, {
   3.5735661271876535`*^9, 3.573566129496457*^9}, 3.5735661858437557`*^9, {
   3.5735662309278355`*^9, 3.573566253079874*^9}, 3.5735662910347404`*^9, {
   3.573566325729202*^9, 3.5735663681612763`*^9}, {3.5735665454711876`*^9, 
   3.573566570072431*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "0.5838141037865535`", ",", "0.6331299197739707`", ",", 
   "0.5082397041819035`", ",", "2.769893665584535`"}], "}"}]], "Output",
 CellChangeTimes->{{3.573566065520745*^9, 3.57356608556678*^9}, {
   3.5735661271876535`*^9, 3.573566129496457*^9}, 3.5735661858437557`*^9, {
   3.5735662309278355`*^9, 3.573566253079874*^9}, 3.5735662910347404`*^9, {
   3.573566325729202*^9, 3.5735663681612763`*^9}, {3.5735665454711876`*^9, 
   3.573566570072431*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"0.563582193744317`", " ", "0.7071067811722829`"}], "-", 
  RowBox[{"1.5940511639711745`", " ", 
   RowBox[{"0.7071067811722829`", "^", "4"}]}]}]], "Input",
 CellChangeTimes->{{3.5735664174261627`*^9, 3.573566460357438*^9}, {
  3.573566491697893*^9, 3.573566528451558*^9}}],

Cell[BoxData[
 RowBox[{"-", "1.6078527398377673`*^-11"}]], "Output",
 CellChangeTimes->{{3.573566435834195*^9, 3.573566460700639*^9}, 
   3.573566528810358*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"For", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"t", "=", 
      RowBox[{"{", 
       RowBox[{"2", ",", "1"}], "}"}]}], ",", 
     RowBox[{"k", "=", "0"}]}], "}"}], ",", 
   RowBox[{
    RowBox[{
     RowBox[{"Abs", "[", 
      RowBox[{
       RowBox[{"t", "[", 
        RowBox[{"[", "1", "]"}], "]"}], "-", 
       RowBox[{"t", "[", 
        RowBox[{"[", "2", "]"}], "]"}]}], "]"}], "\[GreaterEqual]", 
     RowBox[{"10", "^", 
      RowBox[{"-", "6"}]}]}], "&&", 
    RowBox[{"k", "\[LessEqual]", "3000"}]}], ",", 
   RowBox[{"k", "++"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"x", "=", 
      RowBox[{
       RowBox[{"eigx", "[", "y", "]"}], "[", 
       RowBox[{"[", 
        RowBox[{"1", ";;", "dimensionA"}], "]"}], "]"}]}], ",", 
     RowBox[{
      RowBox[{"t", "[", 
       RowBox[{"[", "1", "]"}], "]"}], "=", 
      RowBox[{
       RowBox[{"eigx", "[", "y", "]"}], "[", 
       RowBox[{"[", 
        RowBox[{"-", "1"}], "]"}], "]"}]}], ",", 
     RowBox[{"y", "=", 
      RowBox[{
       RowBox[{"eigy", "[", "x", "]"}], "[", 
       RowBox[{"[", 
        RowBox[{"1", ";;", "dimensionB"}], "]"}], "]"}]}], ",", 
     RowBox[{
      RowBox[{"t", "[", 
       RowBox[{"[", "2", "]"}], "]"}], "=", 
      RowBox[{
       RowBox[{"eigy", "[", "x", "]"}], "[", 
       RowBox[{"[", 
        RowBox[{"-", "1"}], "]"}], "]"}]}], ",", 
     RowBox[{"Print", "[", 
      RowBox[{"x", ",", "y", ",", "t"}], "]"}]}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.573565163807961*^9, 3.5735653923795624`*^9}, {
  3.5735654744357066`*^9, 3.573565549986639*^9}, {3.5735656491716137`*^9, 
  3.5735656629932375`*^9}, {3.573565738824971*^9, 3.5735657395269723`*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
   RowBox[{"{", 
    RowBox[{"0.7071067811874391`", ",", "0.707106781185656`"}], "}"}], 
   "\[InvisibleSpace]", 
   RowBox[{"{", 
    RowBox[{
    "0.5838141076254627`", ",", "0.6331299193647205`", ",", 
     "0.5082397002819703`"}], "}"}], "\[InvisibleSpace]", 
   RowBox[{"{", 
    RowBox[{"2.7698935689111797`", ",", "2.7698935696850677`"}], "}"}]}],
  SequenceForm[{0.7071067811874391, 0.707106781185656}, {0.5838141076254627, 
   0.6331299193647205, 0.5082397002819703}, {2.7698935689111797`, 
   2.7698935696850677`}],
  Editable->False]], "Print",
 CellChangeTimes->{{3.573565663461239*^9, 3.573565678484065*^9}, 
   3.5735657406189737`*^9}]
}, Open  ]]
},
WindowSize->{1424, 805},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"9.0 for Microsoft Windows (32-bit) (2013\:5e742\:670813\
\:65e5)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 749, 21, 92, "Input"],
Cell[1309, 43, 4290, 128, 212, "Input"],
Cell[5602, 173, 1653, 44, 112, "Input"],
Cell[CellGroupData[{
Cell[7280, 221, 6638, 181, 352, "Input"],
Cell[13921, 404, 302, 6, 31, "Output"],
Cell[14226, 412, 1366, 28, 43, "Print"]
}, Open  ]],
Cell[15607, 443, 5462, 167, 172, "Input"],
Cell[21072, 612, 452, 14, 52, "Input"],
Cell[21527, 628, 3745, 96, 132, "Input"],
Cell[25275, 726, 3599, 94, 132, "Input"],
Cell[CellGroupData[{
Cell[28899, 824, 886, 21, 132, "Input"],
Cell[29788, 847, 532, 11, 31, "Output"],
Cell[30323, 860, 464, 9, 31, "Output"],
Cell[30790, 871, 1257, 33, 31, "Output"],
Cell[32050, 906, 492, 9, 63, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[32579, 920, 307, 6, 31, "Input"],
Cell[32889, 928, 160, 3, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[33086, 936, 1716, 51, 52, "Input"],
Cell[34805, 989, 694, 17, 23, "Print"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

