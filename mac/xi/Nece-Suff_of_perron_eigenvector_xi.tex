\documentclass[a4paper]{article}

%\usepackage{ctex}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage{amsfonts}
\usepackage{multirow}
\usepackage{cases}

\usepackage{graphicx}
\usepackage{color}

\newtheorem{Theorem}{Theorem}[section]
\newtheorem{Definition}{Definition}[section]
\newtheorem{Proposition}{Proposition}[section]
\newtheorem{Property}{Property}[section]
\newtheorem{Problem}{Problem}[section]
\newtheorem{Assumption}{Assumption}[section]
\newtheorem{Lemma}{Lemma}[section]
\newtheorem{Corollary}{Corollary}[section]
\newtheorem{Remark}{Remark}[section]
\newtheorem{Example}{Example}[section]
\newtheorem{Algorithm}{Algorithm}[section]
\newtheorem{Conclusion}{Conclusion}[section]

\parskip 0.12in
\setlength{\floatsep}{2\floatsep}
\setlength{\textfloatsep}{2\textfloatsep}
\setlength{\intextsep}{2\intextsep}

\renewcommand{\baselinestretch}{1.2}

\newcommand{\te}[1]{\textbf{\ensuremath{\mathcal{#1}}}}
\newcommand{\di}{$m$ order $n$ dimension}
\newcommand{\edi}[1]{\ensuremath{#1_{i i_2\dots i_m}}}
\newcommand{\edij}[1]{\ensuremath{#1_{ji_2\dots i_m}}}
\newcommand{\edii}[1]{\ensuremath{#1_{i_1i_2\dots i_m}}}
\newcommand{\es}[1]{\ensuremath{#1_{i i \dots i}}}
\newcommand{\su}{\ensuremath{\sum_{i_2,i_3,\dots, i_m=1}^{n}}}

\def \eps{\varepsilon}
\def \btd{\bigtriangledown}
\def \ep{\hbox{ }\hfill$\Box$}
\def \b{\bigtriangledown}
\def \d{\Delta}
\def \Lfr{\Longleftrightarrow}
\def \ra{\rightarrow}

\addtolength{\oddsidemargin}{-0.1 \textwidth}
\addtolength{\textwidth}{0.2 \textwidth}
\addtolength{\topmargin}{-0.1 \textheight}
\addtolength{\textheight}{0.2 \textheight}

\begin{document}
\title{ A necessary and sufficient condition on the existence of positive perron eigenvectors of a nonnegative tensor
\thanks{ corresponding author:\quad Qingzhi Yang}}
\author{
Xi He
\thanks{ \footnotesize{School of Mathematical Sciences and LPMC, Nankai University, Tianjin 300071, P.R. China, Email: heeryerate@gmail.com}} ,
Yiyong Li
\thanks{ \footnotesize{School of Mathematical Sciences and LPMC,Nankai University, Tianjin 300071, P.R. China, Email:haoja625@163.com}},
Qingzhi Yang
\thanks{ \footnotesize{ School of Mathematical Sciences and LPMC, Nankai University, Tianjin 300071, P.R. China,Email: qz-yang@nankai.edu.cn} This author's work is supported by the National Natural Science Foundation of China (Grant No.10871105),the Keygrant Projection of Chinese Ministry of Education(309009)}}

\maketitle

\begin{abstract}
The existence of positive perron eigenvector is an important assumption in several conclusions in tensor theory. In this paper, we first give a necessary and sufficient conditions ensuring the existence of positive perron eigenvector of a tensor. Furthermore, convergence result of NQZ algorithm that depend on the existence of positive perron eigenvector is also given. Next, we develop an useful algorithm mixed partition and perturbation methods to compute the spectral radius and the corresponding eigenvector of a nonnegative tensor. By a tiny modification, we can also give the complexity analysis of our new algorithm. The numerical experiments show that our algorithm could not only compute the spectral radius of a common tensor but its corresponding eigenvector, which is impossible in those prior algorithms. Several applications of our methods in polynomial optimization problems are stated, and we also expand some of the main conclusions in this paper to other types of tensors.

\vspace{3mm}

\noindent {\bf Key words: } Positive perron eigenvector, Spectral radius.\\
{\bf MSC }  74B99, 15A18, 15A69 \hspace{2mm}\vspace{3mm}

\end{abstract}

\newpage

\section{Introduction}
\setcounter{equation}{0} \setcounter{Assumption}{0}
\setcounter{Theorem}{0} \setcounter{Proposition}{0}
\setcounter{Corollary}{0} \setcounter{Lemma}{0}
\setcounter{Definition}{0} \setcounter{Remark}{0}
\setcounter{Algorithm}{0}

In this paper we give a further research on positive perron eigenvector and spectral radius of a nonnegative tensor. This paper is organized as follows. In Section 2, we recall some definitions and results about nonnegative tensors and its spectral radius. In Section 3, two terminologies of tensors are stated and we then study a necessary and sufficient conditions on the existence of the positive perron eigenvector of tensors. By the way, we give some conclusions on the geometric multiplicity of the perron eigenvector in some situations. In Section 4, we discuss several former algorithms, give the convergence result of NQZ algorithm under a more weakly condition. Some numerical experiments are shown to illustrate our points. In Section 4, we also offer a more efficient and effective algorithm to find the spectral radius of a nonnegative tensor and its corresponding eigenvector. With a tiny modification, we can also give the complexity analysis result of our algorithm. Then,in Section 5, some applications in polynomial optimization using our algorithm are stated. In the last section, we expand some main conclusions in this paper in different types of tensor.

We first add a comment on the notation that is used in the sequel. Vectors are written as lowercase letters $(x,y,\ldots)$, matrices correspond to italic capitals $(A,B,\ldots)$, and tensors are written as calligraphic capitals $(\te{A}, \te{B}, \cdots)$. The entry with row index $i$ and column index $j$ in a
matrix $A$, i.e. $A_{ij}$ is symbolized by $a_{ij}$ (also $\te{A}_{i_1\cdots i_m} = a_{i_1\cdots i_m})$. $R^n_+ (R^n_{++})$ denotes the cone $\{x \in R^n\ | x_i\geq(>) 0, i=1,\ldots,n\}$. The symbol $A \geq(>,\leq, <)B$ means that $a_{ij} \geq(>,\leq, <) b_{ij}$ for every $i,j$.

\section{Preliminaries}

First we recall some definitions and results on the nonnegative tensors.

A square tensor is a multidimensional array, and a real order $m$ dimension $n$ tensor $\te{A}$ consists of $n^m$ real entries:
\[
  T_{i_1\cdots i_m} \in R
\]
where $i_j=1,\ldots,n$ for $j=1,\ldots,m$. Like the square matrix, this class of tensors can be regarded as "square" tensors.

In \cite{qi2005} and \cite{lin2005}, we learn that if there are a complex number $\lambda$ and a nonzero complex vector $x$ that are solutions of the following homogeneous polynomial equations:
\[
  \te{A}x^{m-1} = \lambda x^{[m-1]}
\]
then $\lambda$ is called the eigenvalue of $\te{A}$ and $x$ the eigenvector of $\te{A}$ associated with $\lambda$, where $\te{A}x^{m-1}$ and $x^{[m-1]}$ are vectors, whose $i^{th}$ component are
\begin{eqnarray*}
(\te{A}x^{m-1})_i   & = &   \sum^n_{i_2,\ldots,i_m=1}T_{i,i_2\cdots i_m}x_{i_2}\cdots x_{i_m}\\
    (x^{[m-1]})_i   & = &   x^{m-1}_i
\end{eqnarray*}
respectively.  This definition was introduced by Qi \cite{qi2005} where he assumed that $\te{A}$ is an order $m$ dimension $n$ symmetric tensor and $m$ is even. If $\lambda$ and $x$ are restricted in the real field, then $(\lambda,x)$ is called $H$-eigenpaires. Independently, Lim \cite{lin2005} gave such a definition but restricted $x$ to be a real vector and $\lambda$ to be a real number. Here we follow the definition due to Chang et al, where they gave the general definition as above \cite{cpz2008}.

In the following analysis, the notions below may be used.

\begin{Definition}(\cite{yy2010})
The spectral radius of tensor $\te{A}$ is defined as
\[
  \rho(\te{A}) = \max\{|\lambda|: \lambda \textrm{ is an eigenvalue of } \te{A}\}.
\]
\end{Definition}

\begin{Definition}(\cite{lin2005})
A tensor $\te{A} = (T_{i_1\cdots i_m})$ of order $m$ dimension $n$ is called reducible, if there exists a nonempty proper index subset $I \subset \{1,\ldots,n\}$ such that
\[
  T_{i_1\cdots i_m} = 0, \qquad \forall i_1 \in I, \quad \forall i_2,\ldots,i_m \not\in I.
\]
\end{Definition}
If $\te{A}$ is not reducible, then we call $\te{A}$ irreducible.

\begin{Definition}(Definition 2.6  of \cite{cpz2011})
A nonnegative irreducible $m$ order $n$ dimensional tensor $\te{A}$ is called primitive if $F_\te{A}$ does not have a nontrivial invariant set $S$ on $\partial P$. ($\{ 0 \}$ is the trivial invariant set).
\end{Definition}

\begin{Definition}(Definition 2.1 of \cite{ct2009})
A tensor $\te{A} = (\te{A}_{i_1\cdots i_m})$ of order $m$ dimension $n$ is called essentially positive, if $\te{A}x^{m-1} > 0$ for any nonzero $x \ge 0$.
\end{Definition}

\begin{Definition}(Definition 2.1  of \cite{p2010})
A nonnegative matrix M($\te{A}$) is called the majorization associated to nonnegative tensor $\te{A}$, if the $(i,j)^{th}$ element of $M$ ($\te{A}$) is defined to be $a_{ij\cdots j}$ for any $i,j \in{1,\cdots,n}$. $\te{A}$ is called weakly positive if [$M$ ($\te{A}$)]$_{ij}>0$ for all $i \neq j$.
\end{Definition}

\begin{Definition}(Definition 2.5  of \cite{yy22011})
A $n$ order $m$ dimension tensor $\te{A}$ is called weakly reducible, if there exists a nonempty proper index subset $I\subset\{1,\ldots,n\}$ such that
\[
    \te{A}_{i_1\cdots i_m} = 0, \quad \forall i_{1} \in I, \quad \exists i_{j}\not\in I, \quad j=2,\cdots,m.
\]
\end{Definition}

If \te{A} is not weakly reducible, then we call \te{A} weakly irreducible.

The definition 2.8 and the definition 2.9 give the definition of weakly irreducible in different perspectives,while the definition 2.9 is more clear than the definition 2.8.The definition 2.7 and the definition 2.9 play different part in proof.

\begin{Definition}(Definition 3.1 of \cite{cpz2008})
Let $\lambda$ be an eigenvalue of
\[
    \te{A}x^{m-1} =\lambda x^{[m-1]}
\]
We say $\lambda$ has geometric multiplicity q, if the maximum number of linearly in-dependent eigenvectors corresponding to $\lambda$ equals q. If q = 1, then $\lambda$ is called geometrically simple.
\end{Definition}

\begin{Theorem} ( Theorem 2.3 of \cite{yy22011} )
If $\te{A}$ is a nonnegative irreducible tensor of order $m$ dimension $n$ and $\rho(\te{A})$ is the spectral radius, then the following hold:
\begin{description}
  \item[(1)] $\rho(\te{A})>0. $
  \item[(2)] There is a positive eigenvector $x\in R_++^{n}$ corresponding to $\rho(\te{A})$.
  \item[(3)] If $\lambda$ is an eigenvalue with nonnegative eigenvector, then $\lambda=\rho(\te{A})$. Moreover, the nonnegative eigenvector is unique up to a multiplicative constant.
  \item[(4)] If $\lambda$ is an eigenvalue of $\te{A}$, then $|\lambda| \leq \rho(\te{A})$.
\end{description}
\end{Theorem}

\section{A necessary and sufficient condition on the existence of positive perron eigenvectors of a square tensor}
We notice that there are some conclusion built under the assumption that a tensor has a positive perron eigenvector. We state them as follows.

\begin{Theorem}(\cite{yy22011} Theorem 3.3)
Let \te{A} be an order $m$ dimension $n$ nonnegative tensor with a positive eigenvector corresponding to the spectral radius. Then
   \[\min_{x\in \mathbb{R}^+}\max_{\{i|x_i>0\}}\frac {(\te{A}x^{m-1})_i}{x^{[m-1]}_i}=\rho(\te{A})=\max_{x\in \mathbb{R}^+}\min_{\{i|x_i>0\}}\frac {(\te{A}x^{m-1})_i}{x^{[m-1]}_i}\]
\end{Theorem}

\begin{Theorem}(\cite{li2009} Theorem 2.8)
Suppose \te{A} is an $m^{th}-$order $n-$dimensional non-negative tensor with a positive Perron vector, and $\tilde{\te{A}}=\te{A}+\Delta\te{A}$ is the perturbed non-negative tensor of \te{A}. Then we have
\[
  |\rho(\tilde{\te{A}})-\rho(\te{A})|\leqslant\tau(\te{A})||\Delta\te{A}||_{\infty}
\]
where
\[
  \tau\te{A}\equiv\left(\min_{2\leqslant k \leqslant m}\left\{\frac{\max_{1\leqslant i_1,i_2\leqslant n}\sum^n_{\underbrace{i_2,\dots, i_m=1}_{except~i_k}}\te{A}_{i_1,i_2,\dots,i_m}}{\min_{1\leqslant i_1,i_2\leqslant n}\sum^n_{\underbrace{i_2,\dots, i_m=1}_{except~i_k}}\te{A}_{i_1,i_2,\dots,i_m}}\right\}\right)^{m-1}
\]
\end{Theorem}

Thus, we focus on how to check this assumption for any tensor. Simply, there are three cases we should take into account.

At first, from\cite{fgh2011}, we recognize that a weakly irreducible tensor has unique positive eigenvector. Besides, it is easy to judge if a tensor is weakly irreducible or not from the Definition of weakly irreducible and an equivalent condition of nonnegative irreducible tensors as follows.

\begin{Definition}(Definition 2.2  of \cite{hu2011})
Suppose that $\te{A}$  is a nonnegative tensor of order $m$ and dimension $n$.

(1) We call a nonnegative matrix $G(\te{A})$ the representation associated to the nonnegative tensor $\te{A}$, if the $(i,j)^{th}$ element of $G(\te{A})$ is defined to be the summation of $\te{A}_{ii_2\cdots i_m}$ with indices $\{i_2 \cdots i_m\}\ni j$.

(2) We call the tensor $\te{A}$ weakly reducible if its representation  $G(\te{A})$ is a reducible matrix, and weakly primitive if $G(\te{A})$ is a primitive matrix. If $\te{A}$ is not weakly reducible, then it is called weakly irreducible.
\end{Definition}

\begin{Theorem}(\cite{yy2010} Theorem 6.1)
Let $\te{A}\geqslant 0$ be and order $m$ dimension $n$ tensor. Then \te{A} is irreducible if and only if for all $x \in \mathbb{R}^+, x\neq 0$; Let $x_0=x$ and $x_{k+1}=(\te{A}+\te{I})x_k^{m-1}$. Then $x_{n-1}>0$.
\end{Theorem}

Secondly, we focus on a weakly reducible tensor. In \cite{hu2011}, we notice that a weakly reducible tensor can be partition into several blocks that are all induced weakly irreducible tensors.

\begin{Theorem}(\cite{hu2011} Theorem 5.2)
Suppose that \te{A} is a nonnegative tensor of order $m$ and dimension $n$. If \te{A} is weakly reducible, then there is a partition $\{I_1,\dots,I_k\}$ of $\{1,\dots,n\}$ such that every tensor in $\{\te{A}_{I_j}|j\in \{1,\dots,k\}\}$ is weakly irreducible.
\end{Theorem}

Furthermore, we can deduce a further results about eigenvector from that:

\begin{Theorem}
 Given a general tensor that can be partition into several blocks that are all independent with each other. For each $j\in{1,\dots,k}$, if $\rho(\te{A}_{I_j})=\rho(\te{A})>0$, then $x_{I_j}=x_{{j}^*}$ is a nonnegative eigenvector of \te{A}. What's more, if for every $j\in{1,\dots,k}$, there exists $\rho(\te{A}_{I_j})=\rho(\te{A})>0$, then \te{A} has at least one positive eigenvector related to the spectral radius $\rho(\te{A})$.
\end{Theorem}

\text{\textbf{Proof}}\quad
We learn from Theorem 3.4 that if a general tensor can be partition into several blocks blocks that are all independent with each other. Then there exists a partition $\{I_1,\dots,I_k\}$ of $\{1,\dots,n\}$.
\[
  \forall j\in{1,\dots,k},\quad\forall i\in I_j,\qquad\te{A}_{ii_2\dots i_m}=0,\quad \mbox{for all } i_2,\dots,i_k\not \in \{1,\dots,n\}\backslash I_j
\]
Thus, for a $j$ satisfied $\rho(\te{A}_{I_j})=\rho(\te{A})$, we have
\[
  \rho(\te{A})x_{I_j}^{[m-1]}=\te{A}x_{I_j}^{m-1}=\te{A}_{I_j}x_{{j}^*}^{m-1}=\rho(\te{A}_{I_j})x_{{j}^*}^{[m-1]}
\]
Then we learn that $x_{I_j}=x_{{j}^*}>0$ is a nonnegative eigenvector of \te{A}. What' more, if for every $j\in{1,\dots,k}$, there exists $\rho(\te{A}_{I_j})=\rho(\te{A})>0$, we also have
\[
   \forall j\in{1,\dots,k}, x_{I_j}=x_{{j}^*}>0
\]
which means \te{A} has a positive eigenvector $x$.

Therefore, we conclude that in order to hold a positive perron eigenvector, each induced tensor should have the same spectral radius. By the way, in this case, we conclude that the geometric multiplicity $q$ equals to the number of induced tensors whose spectral radius equal to the spectral radius.

Finally, we will discuss the results about the weakly reducible but not necessary in the second conditions. It would be more complicate. We first introduce some basic definition that can be used in the discuss.

\subsection{Definition of basic block and final block}

We found that there are some result in the matrix theory about the existence of positive eigenvector in \cite{non1979}. However, matrix and tensor can not completely analogical. We give the different definition about basic block and final block which are useful in the theorem as follows.

\begin{Definition}
For a order $m$ dimension $n$ nonnegative tensor \te{A}, if we can partition its dimension index $[n]$ into $k$-parts $\{I_1,\dots,I_k\}$ and each induced tensor under $I_j$ is weakly irreducible. Then we call ${I_j}$ a basic block if $\rho(\te{A}_{I_j})=\rho(\te{A})$; We also define a subindex $I_j$ in the partition as final block if for all $i\in I_j$, we have $\te{A}_{ii_2\dots i_m}=0$ when $i_2,\dots,i_k\not \in I_j$.
\end{Definition}

We can simply give an example to illustrate our definition.

\begin{Example}
  Given a order $3$ dimension $4$ nonnegative tensor $\te{A}$, where
  \[\te{A}_{131}=1,\te{A}_{232}=2,\te{A}_{322}=2,\te{A}_{444}=2,t_{ijk}=0, \mbox{otherwise} \quad i,j,k\in\{1,2,3,4\}\]
  use the partition algorithm in \cite{hu2011}, we learn that ${\{1\},\{2,3\},\{4\}}$ are the weakly irreducible partition since the induce tensor
  \[
  \te{A}_{\{1\}}=\te{A}_{111}=0,\qquad \te{A}_{\{4\}}=\te{A}_{444}=0
  \]
  \[
    \te{A}_{\{2,3\}}=(((\te{A}_{222},\te{A}_{223}),(\te{A}_{232},\te{A}_{233})),((\te{A}_{322},\te{A}_{323}),(\te{A}_{332},\te{A}_{333})))=(((0,0),(2,0)),((2,0),(0,0)))
  \]
  are all weakly irreducible tensor. Further, we find that $\rho(\te{A}_{\{2,3\}})=\rho(\te{A}_{\{4\}})=\rho(\te{A})=2$. Thus, according our Definition, ${\{2,3\}},{\{4\}}$ are basic blocks.

  On the other hand, we can find that ${\{2,3\}},{\{4\}}$ also satisfied the Definition of final block. Then we say that ${\{2,3\}},{\{4\}}$ are the final block of \te{A}.

\end{Example}

We state our conclusion as follows.

\begin{Theorem}
To the spectral radius of an order $m$ dimension $n$ nonnegative tensor \te{A}, there corresponds a positive eigenvector of \te{A} if and only if all the final classes of \te{A} are exactly its basic ones.
\end{Theorem}

\text{\textbf{Proof}}\quad Clearly, the weakly irreducible tensor has only one weakly irreducible partition and it is both basic and final. If the tensor \te{A} can be partition into several blocks that are all independent with each other and all its induced tensor's spectral radius equal to \te{A}'s spectral radius, all the partition are both final and basic. It means that this two situation are satisfied the conclusion

\text{\emph{Only if}}\quad We assume that \te{A} has positive perron eigenvector. If $\te{A}_{I_j}$ is the final basic, then we have
\[
   \forall i\in I_j,\quad, \te{A}_{ii_2\dots i_m}=0, \quad \forall i_2,\dots,i_m\not \in I_j
\]
We have $\te{A}x^{m-1}=\rho(\te{A})x^{[m-1]}$. Then for $i\in I_j$
\[
  \begin{split}
    {(\te{A}x^{m-1})}_i & = \su\edi{\te{A}}x_{i_2}\dots x_{i_m}\\
                    & = \sum_{i_2,\dots,i_m\in I_j}\edi{\te{A}}x_{i_2}\dots x_{i_m}\\
                    & = \rho(\te{A}) x_i^{m-1}
                        \end{split}
                        \]
According to the definition of the final basic, $\te{A}_{I_j}$ is weakly irreducible and $x_{I_j}>0$ because $x>0$ is a positive eigenvector. Since we have the Perron-Frobinues theorem for weakly irreducible tensor, we have $\rho(\te{A}_{I_j})=\rho(\te{A})$.  It means that $\te{A}_{I_j}$ is also basic block.

On the other hand, if $\te{A}_{I_j}$ is not the final block, there exist at least an $i\in I_j$ and $\te{A}_{ii_2i_3\dots i_m}>0, i_2,\dots,i_m \not \in I_j$.  Hence,
  \[\begin{split}
    \rho(\te{A}) x_i^{m-1}={(\te{A}x^{m-1})}_i & = \su\edi{\te{A}}x_{i_2}\dots x_{i_m}\\
                    & \geqslant \sum_{i_2,\dots,i_m\in I_j}\edi{\te{A}}x_{i_2}\dots x_{i_m}+\te{A}_{ii_2i_3\dots i_m}x_{i_2}\dots x_{i_m}\\
                    & > (\te{A}_{I_j}x^{m-1})_i
    \end{split}
  \]
  it indicates that $\te{A}_{I_j}x_{I_j}^{m-1}\leqslant\rho(\te{A}) x_{I_j}^{m-1}$
  \[\max_{i\in I_j}\frac {(\te{A}_{I_j}x_{I_j}^{m-1})_i}{(x_{I_j})_i^{m-1}}\leqslant \rho{(\te{A})}\]
  Since $\te{A}_{I_j}$ is weakly irreducible，we have the max-min theorem,
  \[\rho(\te{A}_{I_j})=\min_{x\in \mathbb{R}^+}\max_{x_i>0}\frac {(\te{A}_{I_j}x^{m-1})_i}{x_i^{m-1}}\leqslant\max_{i\in I_j}\frac {(\te{A}_{I_j}x_{I_j}^{m-1})_i}{(x_{I_j})_i^{m-1}}\]
  Thus if $\rho(\te{A}_{I_j})=\rho(\te{A})$，we have $\te{A}_{I_j}x_{I_j}^{m-1}=\rho(\te{A}) x_{I_j}^{m-1}$. It is a contradiction and $\te{A}_{I_j}$ is not the basic block.

 \text{\emph{If}} \quad To prove this conclusion, we first focus on the situation that the tensor that can be partition into two blocks. We denote the corresponding graph as $G(\te{A})$. then we permutate the $G(\te{A})$ as the follow form.
 \[
 \overline{G}(\te{A})=\left(\begin{array}{cc}
                                      G_{11} &  G_{12}\\
                                      0      &  G_{22}
                                      \end{array}
                                      \right)
 \]
 Since we know that the tensor satisfied the final blocks equals to basic blocks. We have the next two situation

 $1.$ we have $G_{11}$ and ${G_{22}}$ are both basic blocks, then we can reach the conclusion that $G_{12}=0$. Which means that
 \[\te{A}x^{m-1}=\left\{
               \begin{array}{ll}
                \te{A}_{I_1}x_{I_1}^{m-1}=\rho(\te{A})x_{I_1}^{[m-1]} & i \in I_1 \\
                \te{A}_{I_2}x_{I_2}^{m-1}=\rho(\te{A})x_{I_2}^{[m-1]} & i \in I_2
               \end{array}
             \right\}=\rho(\te{A})x^{[m-1]}
 \]
According to the definition of basic blocks, we know that $\te{A}_{I_1}$ and $\te{A}_{I_2}$ are both weakly irreducible, then there exist $x_{I_1}>0$ and $x_{I_2}>0$ as their positive eigenvectors. Then, we get that $x=k_1\{x_{I_1},0\}+k_2\{0,x_{I_2}\}$ is the positive eigenvector related to the spectral radius of $\te{A}$.

$2.$ If we have $G_{22}$ is basic blocks while $G_{11}$ is not, then we can reach the conclusion that $G_{12}\neq 0$. We denote that
\[I_1^+=\{i|(G_{12})_{ij}>0,j\in I_2\}, \qquad I_1^-=I_1/I_1^+\]
We know that $G_{11}$ is irreducible, then we know from [strong connectivity], that we can find a circle in $I_1$ and $I_2$ at least, while $G_{12}>0$, for $i\in I_1^+$, we there is some path from $I_1$ to $I_2$. We draw it as follows.
\begin{figure}
\centering
  \includegraphics[scale=0.5]{partition.pdf}
  \caption{graph of $G(\te{A})$}
\end{figure}
where $I_1=\{i,i_2,\cdots,i_r\}$ and $I_2=\{k,k_2,\cdots,k_s\}$, $r=|I_1|,s=|I_2|$.

Consider $G_{22}$, we know that it is both final block and final block. Then we can find its corresponded perron eigenvector, we denote it as $x_{I_2}$ and the spectral radius are $M=\rho(\te{A})$, then $x_{I_2}>0$. In addition, we know from [lemma], $tx_{I_2},t\geq0$ is also the eigenvector of $\te{A}_{I_2}$.
We denote $y(t)$ is a eigenvector, where
\[y(t)_{I_2}=tx_{I_2}\]
for $i\in I_1$, we set \[y(t)_{I_1}=x(t)\].

Since \te{A} is nonnegative tensor, and $\rho(\te{A})=\rho(\te{A}_{I_2})>\rho(\te{A}_{I_1})\geq 0$, we learn from the Lemma, \te{A} has at least a eigenvector $x\geq 0$. Accordingly, there must be exist some $t_0$, where $y(t_0)=\{x(t_0),t_0x_{I_2}\}$ is the eigenvector corresponding the $\rho(\te{A})$. We then try to prove that exist $t_0>0$ and meanwhile we have $x(t_0)>0$.

Hence, we can learn that
\[\te{A}y(t_0)^{m-1}=\left\{
               \begin{array}{ll}
                 (\te{A}_{I_1}x_{I_1}^{m-1})_i+\su\edi{\te{A}}x_{i_2}\dots x_{i_m}, & i\in I_1 \\
                 t_0^{m-1}(\te{A}_{I_2}x_{I_2}^{m-1})_i, & i\in I_2
               \end{array}
             \right\}=\rho(\te{A})y(t_0)^{[m-1]}
\]

Now, we focus on arbitrary $t\in [0,+\infty)$, and find if there is an $f(t)$ satisfied $\te{A}y(t)^{m-1}=f(t)y(t)^{[m-1]}$. It is obvious that if $t=0$, we have $\te{A}y(0)^{m-1}=\te{A}\{x_{0},0\}^T=\{\te{A}_{I_1}x_{0}^{m-1},0\}^T$. Since $\te{A}_{I_1}$ is weakly irreducible, we can find a positive perron eigenvector $x_{0}$ and a corresponding spectral radius $f(0)=\rho(\te{A}_{I_1})<\rho(\te{A})$.

Suppose $\mathbf{P}=(P_{1}\cdots,P_{r})$, $P_{i}(x(t))=(\te{A}_{I_1}x(t)^{m-1})_i+\su\edi{\te{A}}y(t)_{i_2}\dots y(t)_{i_m}$, $i\in I_1$.
Now, we focus on arbitrary $t\in [0,+\infty)$, and define a mapping: $z=f(t)$, $f(t)$ satisfying $P_{i}(x(t))=f(t)x(t)^{[m-1]}_{i}, i\in I_1$ where $x(t)>0$ and $\|x(t)\|_{2}=1$ for $t\in (0,+\infty)$; $t=o$, $z=\rho(\te{A}_{I_1})$. Because $G_{11}$ is irreducible, $\mathbf{P}$ is weakly irreducible. Then we can get that there exists a unique positive $z$ for any $t\in (0,+\infty)$ by Theorem 4.1 in \cite{fgh2011}. For $t\rightarrow t_{0}>0$, $\lim_{t\rightarrow t_{0}}P_{i}(x(t))=f(t_{0})x(t_{0})^{[m-1]}_{i}, i\in I_1$ where $x(t)>0$ and $\|x(t)\|_{2}=1$ and for $t\rightarrow 0$, $\lim_{t\rightarrow 0}P_{i}(x(t))=\rho(\te{A}_{I_1})x(0)^{[m-1]}_{i}, i\in I_1$ where $x(t)>0$ and $\|x(t)\|_{2}=1$. So $\lim_{t\rightarrow 0}f(t)=f(t_{0})$ for $t\in [0,+\infty)$ i.e. $z=f(t)$ is  continuous function for $t\in [0,+\infty)$. Because  $\mathbf{P}$ is weakly irreducible and $I_1^+\neq\emptyset$. So there exists a connected path. Hence we can suppose $a_{i_{k}j_{i_{2}}^{k}\cdots j_{i_{m}}^{k}}>0$, $i_{k+1}\in\{j_{i_{2}}^{k},\cdots,j_{i_{m}}^{k}\}$, $1\leq k\leq r-1$ and $a_{i_{r}j_{i_{2}}^{r}\cdots j_{i_{m}}^{r}}>0$, $I_2\cap\{j_{i_{2}}^{r},\cdots,j_{i_{m}}^{r}\}\neq\emptyset$. Let $p=\min_{1\leq k\leq r}\{a_{i_{k}j_{i_{2}}^{k}\cdots j_{i_{m}}^{k}},1\}$, $M=\max\{\frac{\rho(\te{A})}{p},1\}$, $\xi=(M,M^{m},\cdots,M^{m^{r-2}})$, $t_{1}=M^{m^{r-1}}$. Then $\frac{P_{i}(\xi)}{\xi_{i}^{m-1}}\geq\rho(\te{A})$ and $\frac{P_{i}(\epsilon\xi)}{\epsilon\xi_{i}^{m-1}}\geq\rho(\te{A})$ where $\epsilon\leq 1$. And we can get $x(t_{1})>0$ and $\|x(t_{1})\|_{2}=1$, when $z=f(t_{1})$. So $x(t_{1})<\xi$. Let $\theta=\min_{1\leq i\leq r }\{\frac{x(t_{1})_{i}}{\xi_{i}}\}$ and suppose $\theta=\frac{x(t_{1})_{j}}{\xi_{j}}$. So $x(t_{1})\geq\theta\xi$ and $x(t_{1})_{j}=\theta\xi_{j}$. Then $0\leq P_{i}(x(t_{1}))-P_{i}(\theta\xi)\leq f(t_{1})x(t_{1})^{m-1}_{j}-\rho(\te{A})(\theta\xi_{j})^{m-1}=(f(t_{1})-\rho(\te{A}))x(t_{1})^{m-1}_{j}$. Hence $f(t_{1})\geq\rho(\te{A})$. Then $t_0>0$ and $x(t_0)>0$ can be found satisfying $f(t_{0})=\rho(\te{A})$


We then use induction to prove our conclusion.

At first, we learn that if the tensor can only be partitioned by two parts, our conclusion is hold according to the proof above.

Next, we assume that for any tensor which can be partitioned into $k\geq 2$ parts. In other words, there exists a $k$-partition of $I=\{1,2,\dots,n\}$, where$\bigcup_{j=1}^kI_j=I$ and for any different $1\leq i,j\leq k$, it holds $I_i\cap I_j=\emptyset$. If the final blocks are exact the basic blocks, our result is correct. Then, we prove that if a tensor can be partitioned into $k+1$ parts, our conclusion is also true.

From the [nonnegative matrices]. For any tensor which will be partitioned into $k+1$ parts, we always can find a final block $I_{k+1}$, then we can treat the first $k$ parts as a part $\bar{I}$. Then we get the follow result for the partition.

 \[
 \overline{G}(\te{A})=\left(\begin{array}{cc}
                                      G_{I_{k+1},I_{k+1}} &  G_{I_{k+1},\bar{I}}\\
                                      0      &  G_{\bar{I}\bar{I}}
                                      \end{array}
                                      \right)
 \]

 Since we hold the condition that the final blocks are exact the basic blocks ,then we can confirm that $G_{\bar{I},\bar{I}}$ is definitely a $k$-partition that satisfied our condition. From our assumption, we can learn that $\rho(\te{A}_{\bar{I}})=\rho(\te{A})$ and $\te{A}_{\bar{I}}$ has a correspond eigenvector $x_{\bar{I}}>0$.

 Then under the condition, we have several situations as follows:
 \begin{enumerate}
   \item If $\rho(\te{A}_{I_{k+1}})=\rho(\te{A}_{\bar{I}})=\rho(\te{A})$, then $I_{k+1}$ must be a final block. It can transform to the situation that we can find a positive eigenvector $x=\{x_{I_{k+1}},x_{\bar{I}}\}$
   \item If $\rho(\te{A}_{I_{k+1}})<\rho(\te{A}_{\bar{I}})=\rho(\te{A})$, then $I_{k+1}$ must be not a final block. It means that there are at least one non-zero element in $G_{I_{k+1},\bar{I}}$, from our second part of proof, we can state that the tensor $\te{A}$ also has a positive eigenvector.
\end{enumerate}

Both of these two situation indicate that our conclusion is correct when we can partite our tensor $\te{A}$ into $k+1$ parts. Then for any partitions, we will hold the conclusion in our theorem.

\section{The weakest condition on the convergence of NQZ algorithm}

In this section, we try to prove that the weakest condition on the convergence of NQZ  algorithm is that the tensor needs to have at least one positive eigenvector.

\subsection{convergence of our algorithm}

We learn in \cite{NQZ} that a positive tensor is also a primitive tensor.
\begin{Theorem}(Theorem 3.2)
Let $\te{A}\leqslant 0$ be irreducible. If diag$(\te{A})>0$, then \te{A} is primitive.
\end{Theorem}

\begin{Theorem}
  If tensor $\te{A}$ has at least one positive eigenvector corresponding to the spectral radius. If diag$(\te{A})>0$, then \te{A} is primitive.
\end{Theorem}

\text{\textbf{Proof}}\quad

\subsection{complexity of our algorithm under the perbutation}

In 2009, we know NQZ algorithm in \cite{NQZ} that used for computing the spectral radius of a primitive tensor, which should pay attention on the structure of tensors. We state this algorithm as follows.

\begin{Algorithm}(NQZ)
\begin{description}
       \item[Step 0.] Choose $x^{(0)}\in\mathfrak{R}^n_{++}$, $y^{(0)}=\te{A}(x^{(0)})^{m-1}$, and set $k:=0$
       \item[Step 1.]  Compute
       \[x^{(k+1)}=\frac {(y^{(k)})^{[\frac 1{m-1}]}}{||{(y^{(k)})^{[\frac 1{m-1}]}}||}\qquad y^{(k+1)}=\te{A} (x^{(k+1)})^{m-1}\]
       \[
       \overline{\lambda}_{k+1}=\max_{x^{(k+1)}_i>0}\frac {(y^{(k+1)}_i)}{{x^{(k+1)}_i}^{m-1}},\quad
       \underline{\lambda}_{k+1}=\min_{x^{(k+1)}_i>0}\frac {(y^{(k+1)}_i)}{{x^{(k+1)}_i}^{m-1}}\]

       \item[Step 2.] If $\overline{\lambda}_{k+1}=\underline{\lambda}_{k+1}$, stop. Otherwise, replace $k$ by $k+1$ and go to Step 1.
\end{description}
\end{Algorithm}

in this algorithm, we notice that we use the methods to decrease the dimension of the original tensor. However, we should notice that there are some advantages of this methods, the first is compared with algorithms [] and [], this algorithm can not compute the eigenvector in the not super symmetric tensor. the second is that, if the tensor can not be divide, which indicate that this algorithm can just under the some orginal tensor and has not linear convergence result. Thus, It is necessary for us to test which is the best among those algorithms. we state it as follows.

\subsection{numerical experiment}
We first consider a weakly irreducible situation, which can be solved by NQZ algorithm. we consider the 3-order-10-dimension and list the result as follows.

Then we consider a weakly reducible situation which has a positive eigenvalue, which can not be solve by NQZ algorithm but still can solved by the algorithm, we consider the 3-order-10-dimension and list the result as follows.

At the third, we consider a weakly reducible situation which do not have a positive eigenvalue, which still can not be solve by NQZ algortihm.

Finally, we consider a weakly reducible situation that has spectral radius 0. and found something very stranger.

\begin{Conclusion}
we should notice that ...
\end{Conclusion}

\section{an application and some numerical experiment}

\subsection{on the largest singular of a general tensor }

We intend to use the largest singular of a general tensor to solve some polynomial optimization problem. From the Zhouguanglu, we can do some research on the problem.
\[
   \max \te{A}xxyyzz
\]
\[
   s.t. ||x||_2=||y||_2=||z||_2=1.
\]

\section{some generalization on rectangular and general tensor}

\subsection{rectangular tensor}
\subsection{general tensor}

\section{conclusion}

\textbf{Acknowledgement.} We are grateful to Mr.xihe and
Mr.zhongming chen for them helpful discussion. And the authors would
like to thank the reviewers for their suggestions to improve the
presentation of the paper.

\begin{thebibliography}{99}

\bibitem{cpz2009}K.-C. Chang, K. Pearson and T. Zhang, On eigenvalue problems of real symmetric tensors, \emph{J. Math. Anal. Appl.} 350 (2009), 416-422.
\bibitem{cpz2008}K.-C.Chang, K. Pearson and T. Zhang, Perron Frobenius Theorem for non-negative tensors,\emph{Comm. Math. SciV.} 6 (2008), 507-520.
\bibitem{nqz2009}M. Ng, L. Qi and G. Zhou, Finding the largest eigenvalue of a non-negative tensor,SIAM J. Matrix Anal. Appl.31 (2009),1090-1099.
\bibitem{qsw2007} L. Qi, W. Sun and Y. Wang, Numerical multilinear algebra and its applica-tions,Front. Math. China 2 (2007), 501-526.
\bibitem{bp2009}S.R. Bul\`{o} and M. Pelillo, A generalization of the Motzkin-Straus theorem to hyper-graphs, Optim. Lett. 3 (2009) 187-295.
\bibitem{bp12009} S.R. Bul\`{o} and M. Pelillo, New bounds on the clique number of graphs based on spectral hypergraph theory, T.\"{u}tzle ed., Learning and Intelligent Op-timization, Springer Verlag, Berlin, (2009) 45-48.
\bibitem{p2010} K. Pearson, Essentially positive tensors. \emph{International Journal of Algebra} 4(2010),421-427.
\bibitem{cpz2011} K.-C. Chang, K. Pearson and T. Zhang, Primitivity, the convergence of the NQZ method, and the largest eigenvalue for nonnegative tensors,\emph{SIAM J. Matrix Anal. Appl.} 32 (2011), .
\bibitem{yy2010}Y. Yang and Q. Yang, Further results for Perron-Frobenius Theorem for nonnegative tensors, \emph{SIAM. J. Matrix Anal. Appl.} 31  (2010),  2517-2530.
\bibitem{yy12011}Q. Yang and Y. Yang,On the geometric simplicity of the spectral radius of nonnegative irreducible tensors,http://arxiv.org/abs/1101.2479
\bibitem{qi2005} L. Qi,  Eigenvalues of a real supersymmetric tensor, \emph{J. Symb. Comput.} 40 (2005), 1302-1324.
\bibitem{lin2005} L.-H. Lim,  Singular values and eigenvalues of tensors: a variational approach, \emph{Proceedings of the IEEE International Workshop on Computational Advances in Multi-Sensor Addaptive Processing}, 1 (2005), 129-132.
\bibitem{ct2009} K.C. Chang and T.Zhang, Multiplicity of singular values for tensors, \emph {Commun. Math. Sci.} 7(2009),611-625.
\bibitem{c2009}K.-C. Chang, A nonlinear Krein Rutman theorem, J.Syst.Sci.Complex.22 (2009) 542-554.
\bibitem{fgh2011}S. Friedland, S. Gaubert and L. Han, Perron-Frobenius theorem for nonnegative multilinear forms and extensions, to appear in \emph{Linear Algebra and its Applications}, 2012.
\bibitem{hu2011}S. Hu, Z.-H. Huang and L. Qi, Finding the spectral radius of a nonnegative tensor, http://arxiv.org/abs/1111.2138v1
\bibitem{v2000} R. Varga, Matrix Iterative Analysis, Springer Verlag, (2000).
\bibitem{yy22011}Q. Yang and Y. Yang, Further results for Perron-Frobenius Theorem for nonnegative tensors II,  \emph{SIAM. J. Matrix Anal. Appl.}, 32 (2011), 1236-1250.
\bibitem{yy32011}Y. Yang and Q. Yang, Singular values of nonnegative rectangular tensors, \emph{Front. Math. China}, 6 (2011), 363-378.
\bibitem{zqx20}L. Zhang, L. Qi and Y. Xu. Linear convergence of the LZI algorithm for weakly positive tensors. To appear in: Journal of Computational Mathematics.
\bibitem{li2009}W. Li and MK. NG. The perturbation bound for the spectral radius of a non-negative tensor.
\bibitem{chen2013}Chen Z, Qi L, Yang Q, et al. The solution methods for the largest eigenvalue (singular value) of nonnegative tensors and convergence analysis[J]. Linear Algebra and its Applications, 2013.
\bibitem{non1979}Berman, Abraham, and Robert J. Plemmons. "Nonnegative matrices." The Mathematical Sciences, Classics in Applied Mathematics, 9 (1979).
\bibitem{NQZ}Chang K C, Pearson K J, Zhang T. Primitivity, the convergence of the NQZ method, and the largest eigenvalue for nonnegative tensors[J]. SIAM Journal on Matrix Analysis and Applications, 2011, 32(3): 806-819.
\bibitem{LZI}Zhang L, Qi L, Xu Y. Linear convergence of the LZI algorithm for weakly positive tensors[J]. Journal of Computational Mathematics, 2012, 30(1): 24-33.















\end{thebibliography}

\end{document}
