\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {中文摘要}{}}{I}{Doc-Start}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {Abstract}{}}{II}{Doc-Start}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {Chapter 1}Introduction}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Tensor and its eigenvalues}{1}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}A Brief summary of tensor eigenvalues theory}{2}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Contribution}{3}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}Outline of this thesis}{4}{section.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.5}Notation}{5}{section.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {Chapter 2}$H$-eigenvalues of a nonnegative tensor}{6}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Definition}{6}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Properties and algorithms}{9}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {Chapter 3}A necessary and sufficient condition on the existence of positive eigenvectors}{11}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Motivation}{11}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Definition of basic block and final block}{13}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}A necessary and sufficient condition based on partition}{14}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {Chapter 4}The weakest condition on the convergence of $NQZ$ algorithm}{20}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}The weakest condition on the convergence of $NQZ$ algorithm}{20}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}convergence of our algorithm}{22}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}complexity of our algorithm under the perbutation}{23}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}Numerical experiment}{26}{subsection.4.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {Chapter 5}A improved algorithm for spectral radius based on partition}{29}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Motivation and algorithm}{29}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Complexity of this algorithm under perturbation}{30}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}numerical experiment}{30}{section.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {Chapter 6}conclusion and remark}{35}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {References}{}}{36}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {致谢}{}}{38}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {个人简历}{}}{39}{chapter.6}
